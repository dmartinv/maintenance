package com.daniela.maintenance.web;

import com.daniela.maintenance.models.DepartmentEntity;
import com.daniela.maintenance.models.MachineBrandEntity;
import com.daniela.maintenance.models.WorkplaceEntity;
import com.daniela.maintenance.repository.DepartmentRepository;
import com.daniela.maintenance.repository.MachineBrandRepository;
import com.daniela.maintenance.repository.WorkplaceRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/workplace")
public class WorkplaceController extends AbstractController<WorkplaceEntity, Long>{

    public WorkplaceController(WorkplaceRepository workplaceRepository) {
        super(workplaceRepository);
    }

    /**
     * GET  /workplace/findByName : get all the workplaces filter by denomination.
     *
     * @param name the name of the workplace
     * @return the ResponseEntity with status 200 (OK) and the list of apprentices in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/findByName",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            params = {"name"})
    public ResponseEntity<List<WorkplaceEntity>> findByName(@RequestParam("name") String name)
            throws URISyntaxException {

        List<WorkplaceEntity> workplaceList =
                ((WorkplaceRepository)getJpaRepository())
                        .findFirst8ByNameIgnoreCaseContainingOrderByNameAsc(name);
        return Optional.ofNullable(workplaceList)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

}
