package com.daniela.maintenance.web;

import com.daniela.maintenance.models.MachineFailureEntity;
import com.daniela.maintenance.repository.MachineFailureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/machine_failure")
public class MachineFailureController extends AbstractController<MachineFailureEntity, Long>{
    public MachineFailureController(MachineFailureRepository machineFailureRepository) {
        super(machineFailureRepository);
    }
    
    @RequestMapping(value = "/findByName",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            params = {"name"})
    public ResponseEntity<List<MachineFailureEntity>> findByName(@RequestParam("name") String name)
            throws URISyntaxException {

        List<MachineFailureEntity> machineFailureList =
                ((MachineFailureRepository) getJpaRepository())
                        .findFirst8ByNameIgnoreCaseContainingOrderByNameAsc(name);
        return Optional.ofNullable(machineFailureList)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}

