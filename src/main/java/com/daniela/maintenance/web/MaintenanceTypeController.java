package com.daniela.maintenance.web;

import com.daniela.maintenance.models.MaintenanceTypeEntity;
import com.daniela.maintenance.repository.MaintenanceTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/maintenance_type")
public class MaintenanceTypeController extends AbstractController<MaintenanceTypeEntity, Long>{

    public MaintenanceTypeController(MaintenanceTypeRepository maintenanceTypeRepository) {
        super(maintenanceTypeRepository);
    }

    /**
     * GET  /maintenanceType/findByName : get all the departments filter by denomination.
     *
     * @param name the name of the maintenanceType
     * @return the ResponseEntity with status 200 (OK) and the list of apprentices in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/findByName",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            params = {"name"})
    public ResponseEntity<List<MaintenanceTypeEntity>> findByName(@RequestParam("name") String name)
            throws URISyntaxException {

        List<MaintenanceTypeEntity> maintenanceTypeList =
                ((MaintenanceTypeRepository) getJpaRepository())
                        .findFirst8ByNameIgnoreCaseContainingOrderByNameAsc(name);
        return Optional.ofNullable(maintenanceTypeList)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
