package com.daniela.maintenance.web;

import com.daniela.maintenance.models.BaseEntity;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

public abstract class AbstractController<T, ID extends Serializable> {

    JpaRepository<T, ID> jpaRepository;

    public JpaRepository<T, ID> getJpaRepository() {
        return jpaRepository;
    }

    public void setJpaRepository(JpaRepository<T, ID> jpaRepository) {
        this.jpaRepository = jpaRepository;
    }

    public AbstractController(JpaRepository<T, ID> jpaRepository) {
        this.jpaRepository = jpaRepository;
    }

    @RequestMapping(method= RequestMethod.GET)
    public @ResponseBody
    List<T> listAll() {
        return this.jpaRepository.findAll(new Sort(Sort.Direction.ASC, "createDate"));
    }

    @RequestMapping(value="/{id}", method= RequestMethod.GET)
    public @ResponseBody
        T listAll(@PathVariable(value = "id") ID id) {
        return this.jpaRepository.findOne(id);
    }

    @RequestMapping(method= RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    ResponseEntity<T> create(@RequestBody T json) throws Exception{

        T created = this.jpaRepository.save(json);
        this.jpaRepository.flush();

        return ResponseEntity.ok()
                .body(created);

    }

    @RequestMapping(value="/{id}",
            method=RequestMethod.POST,
            consumes={MediaType.APPLICATION_JSON_VALUE})
    @Transactional
    public @ResponseBody
    ResponseEntity<T> update(@PathVariable ID id, @RequestBody T entity) throws Exception {
        this.jpaRepository.save(entity);
        this.jpaRepository.flush();

        return ResponseEntity.ok()
                .body(entity);
    }

    @RequestMapping(value="/{id}",
            method=RequestMethod.DELETE
    )
    public @ResponseBody Map<String, Object> delete(@PathVariable ID id) throws Exception {
        this.jpaRepository.delete(id);
        Map<String, Object> m = new HashMap();
        m.put("success", true);
        return m;
    }


}
