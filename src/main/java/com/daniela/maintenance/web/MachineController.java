package com.daniela.maintenance.web;

import com.daniela.maintenance.models.MachineBrandEntity;
import com.daniela.maintenance.models.MachineEntity;
import com.daniela.maintenance.repository.MachineBrandRepository;
import com.daniela.maintenance.repository.MachineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/machine")
public class MachineController extends AbstractController<MachineEntity, Long>{
    public MachineController(MachineRepository machineRepository) {
        super(machineRepository);
    }

    /**
     * GET  /machine/findByName : get all the machines filter by denomination.
     *
     * @param name the name of the machine
     * @return the ResponseEntity with status 200 (OK) and the list of apprentices in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/findByName",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            params = {"name"})
    public ResponseEntity<List<MachineEntity>> findByName(@RequestParam("name") String name)
            throws URISyntaxException {

        List<MachineEntity> machineList =
                ((MachineRepository)getJpaRepository())
                        .findFirst8ByFkMachineType_NameIgnoreCaseContainingOrFkMachineBrand_NameIgnoreCaseContainingOrFkMachineCategory_NameIgnoreCaseContaining(
                                name, name, name);
        return Optional.ofNullable(machineList)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
