package com.daniela.maintenance.web;

import com.daniela.maintenance.models.DepartmentEntity;
import com.daniela.maintenance.models.EmployeeEntity;
import com.daniela.maintenance.repository.DepartmentRepository;
import com.daniela.maintenance.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/employee")
public class EmployeeController extends AbstractController<EmployeeEntity, Long>{

    public EmployeeController(EmployeeRepository employeeRepository) {
        super(employeeRepository);
    }

    /**
     * GET  /employee/findByName : get all the departments filter by denomination.
     *
     * @param name the name of the employee
     * @return the ResponseEntity with status 200 (OK) and the list of apprentices in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/findByName",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            params = {"name"})
    public ResponseEntity<List<EmployeeEntity>> findByName(@RequestParam("name") String name)
            throws URISyntaxException {

        List<EmployeeEntity> employeeList =
                ((EmployeeRepository) getJpaRepository())
                        .findFirst8ByNameIgnoreCaseContainingOrderByNameAsc(name);
        return Optional.ofNullable(employeeList)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    /**
     * GET  /employee/findByName : get all the departments filter by denomination.
     *
     * @param name the name of the employee
     * @return the ResponseEntity with status 200 (OK) and the list of apprentices in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/findByNameAndJobPosition",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EmployeeEntity>> findByName(@RequestParam("name") String name,
                                                           @RequestParam("jobPosition") String jobPosition)
            throws URISyntaxException {

        List<EmployeeEntity> employeeList =
                ((EmployeeRepository) getJpaRepository())
                        .findFirst8ByNameIgnoreCaseContainingAndJobPositionOrderByNameAsc(name, jobPosition);
        return Optional.ofNullable(employeeList)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }



    /**
     * GET  /employee/findByJobPosition : get all the departments filter by denomination.
     *
     * @param jobPosition the name of the employee
     * @return the ResponseEntity with status 200 (OK) and the list of apprentices in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/findByJobPosition",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EmployeeEntity>> findByJobPosition(@RequestParam("jobPosition") String jobPosition)
            throws URISyntaxException {

        List<EmployeeEntity> employeeList =
                ((EmployeeRepository) getJpaRepository())
                        .findByJobPosition(jobPosition);
        return Optional.ofNullable(employeeList)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
