package com.daniela.maintenance.web;

import com.daniela.maintenance.models.MachineBrandEntity;
import com.daniela.maintenance.models.MachineBrandEntity;
import com.daniela.maintenance.repository.MachineBrandRepository;
import com.daniela.maintenance.repository.MachineBrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/machine_brand")
public class MachineBrandController extends AbstractController<MachineBrandEntity, Long>{
    public MachineBrandController(MachineBrandRepository machineBrandRepository) {
        super(machineBrandRepository);
    }

    /**
     * GET  /machine_brand/findByName : get all the machineBrands filter by denomination.
     *
     * @param name the name of the machineBrand
     * @return the ResponseEntity with status 200 (OK) and the list of apprentices in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/findByName",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            params = {"name"})
    public ResponseEntity<List<MachineBrandEntity>> findByName(@RequestParam("name") String name)
            throws URISyntaxException {

        List<MachineBrandEntity> machineBrandList =
                ((MachineBrandRepository)getJpaRepository())
                        .findFirst8ByNameIgnoreCaseContainingOrderByNameAsc(name);
        return Optional.ofNullable(machineBrandList)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
