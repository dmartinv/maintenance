package com.daniela.maintenance.web;

import com.daniela.maintenance.models.MaintenanceTaskEntity;
import com.daniela.maintenance.models.MaintenanceTaskEntity;
import com.daniela.maintenance.repository.MaintenanceTaskRepository;
import com.daniela.maintenance.repository.MaintenanceTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/maintenance_task")
public class MaintenanceTaskController extends AbstractController<MaintenanceTaskEntity, Long>{
    public MaintenanceTaskController(MaintenanceTaskRepository maintenanceTaskRepository) {
        super(maintenanceTaskRepository);
    }

    /**
     * GET  /maintenance_task/findByName : get all the maintenanceTasks filter by denomination.
     *
     * @param fkMaintenance the fkMaintenance of the maintenanceTask
     * @return the ResponseEntity with status 200 (OK) and the list of apprentices in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/findByFkMaintenance",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
        )
    public ResponseEntity<List<MaintenanceTaskEntity>> findByName(@RequestParam("fkMaintenance") Long fkMaintenance)
            throws URISyntaxException {

        List<MaintenanceTaskEntity> maintenanceTaskList =
                ((MaintenanceTaskRepository)getJpaRepository())
                        .findByFkMaintenance_id(fkMaintenance);
        return Optional.ofNullable(maintenanceTaskList)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

}
