package com.daniela.maintenance.web;

import com.daniela.maintenance.models.MaintenanceEntity;
import com.daniela.maintenance.models.MaintenanceEntity;
import com.daniela.maintenance.repository.MaintenanceRepository;
import com.daniela.maintenance.repository.MaintenanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.URISyntaxException;
import java.util.*;

@Controller
@RequestMapping("/api/maintenance")
public class MaintenanceController extends AbstractController<MaintenanceEntity, Long>{
    public MaintenanceController(MaintenanceRepository maintenanceRepository) {
        super(maintenanceRepository);
    }

    /**
     * GET  /maintenance/findByMechanic : get all the departments filter by denomination.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of apprentices in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/findByMechanic",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
            )
    public ResponseEntity<ArrayList<HashMap<String,Object>>> findByMechanic()
            throws URISyntaxException {

        ArrayList<HashMap<String,Object>> maintenanceList =
                ((MaintenanceRepository) getJpaRepository())
                        .findByEmployee();
        return Optional.ofNullable(maintenanceList)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
