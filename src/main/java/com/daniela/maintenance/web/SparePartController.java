package com.daniela.maintenance.web;

import com.daniela.maintenance.models.DepartmentEntity;
import com.daniela.maintenance.models.SparePartEntity;
import com.daniela.maintenance.repository.DepartmentRepository;
import com.daniela.maintenance.repository.SparePartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/spare_part")
public class SparePartController extends AbstractController<SparePartEntity, Long>{

    public SparePartController(SparePartRepository sparePartRepository) {
        super(sparePartRepository);
    }

    /**
     * GET  /apprentices/findByName : get all the apprentices filter by denomination.
     *
     * @param name the name of the apprentices
     * @return the ResponseEntity with status 200 (OK) and the list of apprentices in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/findByName",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            params = {"name"})
    public ResponseEntity<List<SparePartEntity>> findByName(@RequestParam("name") String name)
            throws URISyntaxException {

        List<SparePartEntity> sparePartList =
                ((SparePartRepository)getJpaRepository())
                        .findFirst8ByNameIgnoreCaseContainingOrderByNameAsc(name);
        return Optional.ofNullable(sparePartList)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

}
