package com.daniela.maintenance.web;

import com.daniela.maintenance.models.RequestSparePartEntity;
import com.daniela.maintenance.repository.RequestSparePartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/request_spare_part")
public class RequestSparePartController extends AbstractController<RequestSparePartEntity, Long>{

    public RequestSparePartController(RequestSparePartRepository requestSparePartRepository) {
        super(requestSparePartRepository);
    }
}
