package com.daniela.maintenance.repository;

import com.daniela.maintenance.models.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {
    List<EmployeeEntity>
    findFirst8ByNameIgnoreCaseContainingOrderByNameAsc(String name);

    List<EmployeeEntity>
    findFirst8ByNameIgnoreCaseContainingAndJobPositionOrderByNameAsc(String name, String jobPosition);

    List<EmployeeEntity>
            findByJobPosition(String jobPosition);
}
