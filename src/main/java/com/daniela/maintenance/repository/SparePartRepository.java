package com.daniela.maintenance.repository;

import com.daniela.maintenance.models.DepartmentEntity;
import com.daniela.maintenance.models.SparePartEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SparePartRepository extends JpaRepository<SparePartEntity, Long> {
    List<SparePartEntity>
    findFirst8ByNameIgnoreCaseContainingOrderByNameAsc(String name);
}
