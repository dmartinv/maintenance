package com.daniela.maintenance.repository;

import com.daniela.maintenance.models.DepartmentEntity;
import com.daniela.maintenance.models.MachineBrandEntity;
import com.daniela.maintenance.models.WorkplaceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WorkplaceRepository extends JpaRepository<WorkplaceEntity, Long> {
    List<WorkplaceEntity>
    findFirst8ByNameIgnoreCaseContainingOrderByNameAsc(String name);
}
