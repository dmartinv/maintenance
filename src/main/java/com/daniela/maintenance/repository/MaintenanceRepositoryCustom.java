package com.daniela.maintenance.repository;

import com.daniela.maintenance.models.MaintenanceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.*;

public interface MaintenanceRepositoryCustom{
    ArrayList<HashMap<String,Object>>
        findByEmployee();
}
