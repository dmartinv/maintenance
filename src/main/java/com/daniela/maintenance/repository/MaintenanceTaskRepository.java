package com.daniela.maintenance.repository;

import com.daniela.maintenance.models.DepartmentEntity;
import com.daniela.maintenance.models.MaintenanceTaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MaintenanceTaskRepository extends JpaRepository<MaintenanceTaskEntity, Long> {
    List<MaintenanceTaskEntity>
        findByFkMaintenance_id(Long id);
}
