package com.daniela.maintenance.repository;

import com.daniela.maintenance.models.MaintenanceTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MaintenanceTypeRepository extends JpaRepository<MaintenanceTypeEntity, Long> {
    List<MaintenanceTypeEntity>
    findFirst8ByNameIgnoreCaseContainingOrderByNameAsc(String name);
}
