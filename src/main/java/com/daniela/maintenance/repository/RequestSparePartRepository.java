package com.daniela.maintenance.repository;

import com.daniela.maintenance.models.RequestSparePartEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestSparePartRepository extends JpaRepository<RequestSparePartEntity, Long> {
}
