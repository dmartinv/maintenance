package com.daniela.maintenance.repository;

import com.daniela.maintenance.models.MachineFailureEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MachineFailureRepository extends JpaRepository<MachineFailureEntity, Long> {
    List<MachineFailureEntity>
    findFirst8ByNameIgnoreCaseContainingOrderByNameAsc(String name);
}
