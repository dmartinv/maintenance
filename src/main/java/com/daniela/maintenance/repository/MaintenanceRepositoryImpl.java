package com.daniela.maintenance.repository;

import com.daniela.maintenance.models.MaintenanceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.*;

public class MaintenanceRepositoryImpl implements MaintenanceRepositoryCustom {


    @PersistenceContext
    private EntityManager em;

    @Override
    public ArrayList<HashMap<String,Object>>
        findByEmployee(){

        String queryString = "select employee.name, employee.last_name, count(*) from employee, maintenance where employee.id = maintenance.fk_employee and employee.job_position = \'Mecánico\' group by employee.name, employee.last_name";

        Query query = this.em.createNativeQuery(queryString);

        List<Object[]> array2 = (List<Object[]> )query.getResultList();
        ArrayList<HashMap<String,Object>> arrayListString = new ArrayList<HashMap<String,Object>>();
        for(Object[] object: array2){
            HashMap<String,Object> obj = new HashMap<String,Object>();
            obj.put("name", String.valueOf(object[0])) ;
            obj.put("lastName", String.valueOf(object[1])) ;
            obj.put("count", String.valueOf(object[2]));
            arrayListString.add(obj);
        }
        return arrayListString;
    }

}
