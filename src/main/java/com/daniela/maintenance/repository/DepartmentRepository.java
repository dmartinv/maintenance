package com.daniela.maintenance.repository;

import com.daniela.maintenance.models.DepartmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DepartmentRepository extends JpaRepository<DepartmentEntity, Long> {
    List<DepartmentEntity>
        findFirst8ByNameIgnoreCaseContainingOrderByNameAsc(String name);
}
