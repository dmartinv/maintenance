package com.daniela.maintenance.repository;

import com.daniela.maintenance.models.MachineEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MachineRepository extends JpaRepository<MachineEntity, Long> {
    List<MachineEntity>
        findFirst8ByFkMachineType_NameIgnoreCaseContainingOrFkMachineBrand_NameIgnoreCaseContainingOrFkMachineCategory_NameIgnoreCaseContaining
            (String machineName, String machineBrand, String machineCategory);
}
