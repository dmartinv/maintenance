package com.daniela.maintenance.repository;

import com.daniela.maintenance.models.DepartmentEntity;
import com.daniela.maintenance.models.MachineBrandEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MachineBrandRepository extends JpaRepository<MachineBrandEntity, Long> {
    List<MachineBrandEntity>
        findFirst8ByNameIgnoreCaseContainingOrderByNameAsc(String name);

}
