package com.daniela.maintenance.repository;

import com.daniela.maintenance.models.MachineBrandEntity;
import com.daniela.maintenance.models.MachineCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MachineCategoryRepository extends JpaRepository<MachineCategoryEntity, Long> {
    List<MachineCategoryEntity>
    findFirst8ByNameIgnoreCaseContainingOrderByNameAsc(String name);
}
