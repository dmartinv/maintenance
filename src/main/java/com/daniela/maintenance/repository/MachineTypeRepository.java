package com.daniela.maintenance.repository;

import com.daniela.maintenance.models.MachineBrandEntity;
import com.daniela.maintenance.models.MachineTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MachineTypeRepository extends JpaRepository<MachineTypeEntity, Long> {
    List<MachineTypeEntity>
    findFirst8ByNameIgnoreCaseContainingOrderByNameAsc(String name);
}
