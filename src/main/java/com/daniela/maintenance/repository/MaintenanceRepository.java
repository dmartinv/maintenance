package com.daniela.maintenance.repository;

import com.daniela.maintenance.models.MaintenanceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface MaintenanceRepository extends JpaRepository<MaintenanceEntity, Long>, MaintenanceRepositoryCustom {
}
