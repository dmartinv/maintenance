package com.daniela.maintenance.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = MaintenanceTypeEntity.TABLE)
public class MaintenanceTypeEntity extends NamedEntity{

    public static final String TABLE = "maintenance_type";

    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_STANDARD_HOURS = "standard_hours";


    @Column(name = KEY_DESCRIPTION)
    private String description;

    @Column(name = KEY_STANDARD_HOURS)
    private Integer standard_hours;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStandardHours() {
        return standard_hours;
    }

    public void setStandard_hours(Integer standard_hours) {
        this.standard_hours = standard_hours;
    }
}
