package com.daniela.maintenance.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = RequestSparePartEntity.TABLE)
public class RequestSparePartEntity extends BaseEntity{

    public static final String TABLE = "request_spare_part";

    public static final String KEY_REQUEST_DATE = "request_date";
    public static final String KEY_DELIVERY_DATE = "delivery_date";
    public static final String KEY_FK_MACHINE = "fk_machine";
    public static final String KEY_FK_SPARE_PART = "fk_spare_part";
    public static final String KEY_UNITS = "units";


    @Column(name = KEY_REQUEST_DATE)
    private Date requestDate;

    @Column(name = KEY_DELIVERY_DATE)
    private Date deliveryDate;

    @Column(name = KEY_UNITS)
    private Integer units;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = KEY_FK_MACHINE)
    //@JsonBackReference("fkMachine")
    MachineEntity fkMachine;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = KEY_FK_SPARE_PART)
    //@JsonBackReference("fkSparePart")
    SparePartEntity fkSparePart;

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Integer getUnits() {
        return units;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public MachineEntity getFkMachine() {
        return fkMachine;
    }

    public void setFkMachine(MachineEntity fkMachine) {
        this.fkMachine = fkMachine;
    }

    public SparePartEntity getFkSparePart() {
        return fkSparePart;
    }

    public void setFkSparePart(SparePartEntity fkSparePart) {
        this.fkSparePart = fkSparePart;
    }
}
