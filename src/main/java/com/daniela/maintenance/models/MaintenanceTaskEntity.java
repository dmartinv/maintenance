package com.daniela.maintenance.models;

import javax.persistence.*;

@Entity
@Table(name = MaintenanceTaskEntity.TABLE)
public class MaintenanceTaskEntity extends NamedEntity{

    public static final String TABLE = "maintenance_task";

    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_FK_MAINTENANCE = "fk_maintenance";

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = KEY_FK_MAINTENANCE)
    //@JsonBackReference
    MaintenanceEntity fkMaintenance;

    @Column(name = KEY_DESCRIPTION)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MaintenanceEntity getFkMaintenance() {
        return fkMaintenance;
    }

    public void setFkMaintenance(MaintenanceEntity fkMaintenance) {
        this.fkMaintenance = fkMaintenance;
    }
}
