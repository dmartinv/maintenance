package com.daniela.maintenance.models;

import javax.persistence.*;

@Entity
@Table(name = DepartmentEntity.TABLE)
public class DepartmentEntity extends NamedEntity{

    public static final String TABLE = "department";

}
