package com.daniela.maintenance.models;

import java.util.Set;

public interface InterfaceMaintenanceEntity {
    Set<MaintenanceTaskEntity> getMaintenanceTaskEntityList();

    void setMaintenanceTaskEntityList(Set<MaintenanceTaskEntity> maintenanceTaskEntityList);
}
