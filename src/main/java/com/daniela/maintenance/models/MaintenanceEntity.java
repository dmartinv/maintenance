package com.daniela.maintenance.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.util.Set;
import java.util.Date;

@Entity
@Table(name = MaintenanceEntity.TABLE)
public class MaintenanceEntity extends NamedEntity {

    public static final String TABLE = "maintenance";

    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_PRIORITY = "priority";
    public static final String KEY_STATUS = "status";
    public static final String KEY_STANDARD_HOURS = "standard_hours";
    public static final String KEY_DUE_DATE = "due_date";
    public static final String KEY_START_DATE = "start_date";
    public static final String KEY_FK_EMPLOYEE = "fk_employee";
    public static final String KEY_FK_MACHINE_FAILURE = "fk_machine_failure";
    public static final String KEY_FK_MAINTENANCE_TYPE = "fk_maintenance_type";
    public static final String KEY_FK_MACHINE = "fk_machine";


    @Column(name = KEY_DESCRIPTION)
    private String description;

    @Column(name = KEY_PRIORITY)
    private String priority;

    @Column(name = KEY_STATUS)
    private String status;

    @Column(name = KEY_STANDARD_HOURS)
    private Integer standardHours;

    @Column(name = KEY_DUE_DATE)
    private Date dueDate;

    @Column(name = KEY_START_DATE)
    private Date startDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = KEY_FK_MACHINE)
    MachineEntity fkMachine;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = KEY_FK_MACHINE_FAILURE)
    MachineFailureEntity fkMachineFailure;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = KEY_FK_MAINTENANCE_TYPE)
    MaintenanceTypeEntity fkMaintenanceType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = KEY_FK_EMPLOYEE)
    EmployeeEntity fkEmployee;

/*
    @OneToMany(targetEntity = MaintenanceTaskEntity.class,  fetch=FetchType.EAGER,
            mappedBy = "fkMaintenance", cascade = CascadeType.ALL, orphanRemoval=true)
    private Set<MaintenanceTaskEntity> maintenanceTaskEntityList;
*/

    public String getDescription() {
        return description;
    }

    public void setPriorityStatus(){
        this.priority = "low";
        this.status = "created";
    }

    public void setPriorityStatus(String priority){
        this.priority = priority;
        this.status = "created";
    }

    public void setPriorityStatus(String priority, String status){
        this.priority = priority;
        this.status = status;
    }


    public void setDescription(String description) {
        this.description = description;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStandardHours() {
        return standardHours;
    }

    public void setStandardHours(Integer standardHours) {
        this.standardHours = standardHours;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public EmployeeEntity getFkEmployee() {
        return fkEmployee;
    }

    public void setFkEmployee(EmployeeEntity fkEmployee) {
        this.fkEmployee = fkEmployee;
    }

    public MachineFailureEntity getFkMachineFailure() {
        return fkMachineFailure;
    }

    public void setFkMachineFailure(MachineFailureEntity fkMachineFailure) {
        this.fkMachineFailure = fkMachineFailure;
    }

    public MaintenanceTypeEntity getFkMaintenanceType() {
        return fkMaintenanceType;
    }

    public void setFkMaintenanceType(MaintenanceTypeEntity fkMaintenanceType) {
        this.fkMaintenanceType = fkMaintenanceType;
    }

    public MachineEntity getFkMachine() {
        return fkMachine;
    }

    public void setFkMachine(MachineEntity fkMachine) {
        this.fkMachine = fkMachine;
    }

    /*
    @Override
    public Set<MaintenanceTaskEntity> getMaintenanceTaskEntityList() {
        return maintenanceTaskEntityList;
    }

    @Override
    public void setMaintenanceTaskEntityList(Set<MaintenanceTaskEntity> maintenanceTaskEntityList) {
        this.maintenanceTaskEntityList = maintenanceTaskEntityList;
    }*/
}
