package com.daniela.maintenance.models;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = MachineEntity.TABLE)
public class MachineEntity extends NamedEntity{

    public static final String TABLE = "machine";

    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_SERIAL_NUMBER = "serial_number";
    public static final String KEY_MODEL = "model";
    public static final String KEY_ADQUISITION_DATE = "adquisition_date";
    public static final String KEY_VOLTAGE = "voltage";
    public static final String KEY_FK_MACHINE_TYPE = "fk_machine_type";
    public static final String KEY_FK_MACHINE_BRAND = "fk_machine_brand";
    public static final String KEY_FK_MACHINE_CATEGORY = "fk_machine_category";
    public static final String KEY_FK_WORKPLACE = "fk_workplace";


    @Column(name = KEY_DESCRIPTION)
    private String description;

    @Column(name = KEY_SERIAL_NUMBER)
    private String serialNumber;

    @Column(name = KEY_MODEL)
    private String model;

    @Column(name = KEY_ADQUISITION_DATE)
    @Temporal(TemporalType.DATE)
    Date adquisitionDate;

    @Column(name = KEY_VOLTAGE)
    Double voltage;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = KEY_FK_MACHINE_TYPE)
    MachineTypeEntity fkMachineType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = KEY_FK_MACHINE_BRAND)
    MachineBrandEntity fkMachineBrand;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = KEY_FK_MACHINE_CATEGORY)
    MachineCategoryEntity fkMachineCategory;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = KEY_FK_WORKPLACE)
    WorkplaceEntity fkWorkplace;

    /*
    @OneToMany(targetEntity = RequestSparePartEntity.class,  fetch=FetchType.EAGER,
            mappedBy = "fkMachine", cascade = CascadeType.ALL, orphanRemoval=true)
    private Set<RequestSparePartEntity> requestSparePartEntityList;
    */

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Date getAdquisitionDate() {
        return adquisitionDate;
    }

    public void setAdquisitionDate(Date adquisitionDate) {
        this.adquisitionDate = adquisitionDate;
    }

    public Double getVoltage() {
        return voltage;
    }

    public void setVoltage(Double voltage) {
        this.voltage = voltage;
    }

    public MachineTypeEntity getFkMachineType() {
        return fkMachineType;
    }

    public void setFkMachineType(MachineTypeEntity fkMachineType) {
        this.fkMachineType = fkMachineType;
    }

    public MachineBrandEntity getFkMachineBrand() {
        return fkMachineBrand;
    }

    public void setFkMachineBrand(MachineBrandEntity fkMachineBrand) {
        this.fkMachineBrand = fkMachineBrand;
    }

    public MachineCategoryEntity getFkMachineCategory() {
        return fkMachineCategory;
    }

    public void setFkMachineCategory(MachineCategoryEntity fkMachineCategory) {
        this.fkMachineCategory = fkMachineCategory;
    }

    public WorkplaceEntity getFkWorkplace() {
        return fkWorkplace;
    }

    public void setFkWorkplace(WorkplaceEntity fkWorkplace) {
        this.fkWorkplace = fkWorkplace;
    }

    /*
    public Set<RequestSparePartEntity> getRequestSparePartEntityList() {
        return requestSparePartEntityList;
    }

    public void setRequestSparePartEntityList(Set<RequestSparePartEntity> requestSparePartEntityList) {
        this.requestSparePartEntityList = requestSparePartEntityList;
    }
    */
}
