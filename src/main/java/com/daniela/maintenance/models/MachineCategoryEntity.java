package com.daniela.maintenance.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = MachineCategoryEntity.TABLE)
public class MachineCategoryEntity extends NamedEntity{

    public static final String TABLE = "machine_category";

    public static final String KEY_DESCRIPTION = "description";


    @Column(name = KEY_DESCRIPTION)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
