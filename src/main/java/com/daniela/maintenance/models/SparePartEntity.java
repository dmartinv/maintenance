package com.daniela.maintenance.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = SparePartEntity.TABLE)
public class SparePartEntity extends NamedEntity{

    public static final String TABLE = "spare_part";

    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_FK_MACHINE_TYPE = "fk_machine_type";


    @Column(name = KEY_DESCRIPTION)
    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = KEY_FK_MACHINE_TYPE)
    /*@JsonBackReference*/
    MachineTypeEntity fkMachineType;

    /*
    @OneToMany(targetEntity = RequestSparePartEntity.class,  fetch= FetchType.EAGER,
            mappedBy = "fkSparePart", cascade = CascadeType.ALL, orphanRemoval=true)
    private Set<RequestSparePartEntity> requestSparePartEntityList;
    */

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MachineTypeEntity getFkMachineType() {
        return fkMachineType;
    }

    public void setFkMachineType(MachineTypeEntity fkMachineType) {
        this.fkMachineType = fkMachineType;
    }


    /*
    public Set<RequestSparePartEntity> getRequestSparePartEntityList() {
        return requestSparePartEntityList;
    }

    public void setRequestSparePartEntityList(Set<RequestSparePartEntity> requestSparePartEntityList) {
        this.requestSparePartEntityList = requestSparePartEntityList;
    }
    */
}
