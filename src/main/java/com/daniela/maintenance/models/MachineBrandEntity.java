package com.daniela.maintenance.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = MachineBrandEntity.TABLE)
public class MachineBrandEntity extends NamedEntity{

    public static final String TABLE = "machine_brand";

}
