package com.daniela.maintenance.models;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public abstract class BaseEntity implements InterfaceBaseEntity{

    public static final String KEY_ID = "id";
    public static final String KEY_WRITE_DATE = "write_date";
    public static final String KEY_CREATE_DATE = "create_date";
    public static final String KEY_LAST_MODIFIED_DATE = "last_modified_date";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = KEY_CREATE_DATE)
    private Date createDate  = new Date();

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = KEY_LAST_MODIFIED_DATE)
    private Date lastModifiedDate = new Date();


    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public boolean isNew() {
        return (this.id == null);
    }


    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String TABLE_NAME(){
        return "BaseEntity";
    }


}
