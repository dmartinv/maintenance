package com.daniela.maintenance.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = MachineTypeEntity.TABLE)
public class MachineTypeEntity extends NamedEntity{

    public static final String TABLE = "machine_type";

    public static final String KEY_DESCRIPTION = "description";


    @Column(name = KEY_DESCRIPTION)
    private String description;

    /*
    @OneToMany(targetEntity = SparePartEntity.class,  fetch= FetchType.EAGER,
            mappedBy = "fkMachineType", cascade = CascadeType.ALL, orphanRemoval=true)
    private Set<SparePartEntity> sparePartEntityList;
*/
    public String getDescription() {
        return description;
    }
/*
    public Set<SparePartEntity> getSparePartEntityList() {
        return sparePartEntityList;
    }

    public void setSparePartEntityList(Set<SparePartEntity> sparePartEntityList) {
        this.sparePartEntityList = sparePartEntityList;
    }
*/
    public void setDescription(String description) {
        this.description = description;
    }
}
