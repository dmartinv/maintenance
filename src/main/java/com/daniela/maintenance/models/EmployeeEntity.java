package com.daniela.maintenance.models;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = EmployeeEntity.TABLE)
public class EmployeeEntity extends NamedEntity{

    public static final String TABLE = "employee";

    public static final String KEY_IDENTIFICATION = "identification";
    public static final String KEY_LAST_NAME = "last_name";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_JOB_POSITION = "job_position";
    public static final String KEY_SALARY = "salary";
    public static final String KEY_FK_DEPARTMENT = "fk_department";


    @Column(name = KEY_LAST_NAME)
    private String lastName;

    @Column(name = KEY_IDENTIFICATION)
    private String identification;

    @Column(name = KEY_GENDER)
    private String gender;

    @Column(name = KEY_JOB_POSITION)
    private String jobPosition;

    @Column(name = KEY_SALARY)
    Double salary;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = KEY_FK_DEPARTMENT)
    DepartmentEntity fkDepartment;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public void setPosition(String jobPositionName) {
        if(true){}
        this.jobPosition = jobPositionName;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public DepartmentEntity getFkDepartment() {
        return fkDepartment;
    }

    public void setFkDepartment(DepartmentEntity fkDepartment) {
        this.fkDepartment = fkDepartment;
    }
}
