package com.daniela.maintenance.models;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = WorkplaceEntity.TABLE)
public class WorkplaceEntity extends NamedEntity{

    public static final String TABLE = "work_place";

}
