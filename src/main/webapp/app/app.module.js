(function() {
    'use strict';

    angular
        .module('maintenanceApp', [
            'ngResource',
            'ui.router',
            'ui.grid',
            'ui.bootstrap',
            'ui.bootstrap.datetimepicker',
            'nvd3'
        ]).run(['$state', '$rootScope' , function ($state, $rootScope) {
            $state.transitionTo('root');

            //array to hold the alerts to be displayed on the page
            $rootScope.alerts = [];

            /**
             *This function is used to push alerts onto the alerts array.
             */
            $rootScope.addAlert = function(type, message) {

                //add the new alert into the array of alerts to be displayed.
                $rootScope.alerts.push({type: type, msg: message});
            };

            /**
             *This function closes the alert
             */
            $rootScope.closeAlert = function(index) {

                //remove the alert from the array to avoid showing previous alerts
                $rootScope.alerts.splice(0);
            };
        }]
    );


})();

function convertLocalDateFromServer (date) {
    if (date) {
        var dateString = date.split('-');
        return new Date(dateString[0], dateString[1] - 1, dateString[2]);
    }
    return null;
}

function isNull(variable) {
    return typeof variable === 'undefined' || variable === null;
}
