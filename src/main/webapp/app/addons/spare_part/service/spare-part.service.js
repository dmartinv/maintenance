
(function() {
    'use strict';
    var app = angular.module("maintenanceApp");
    app.factory("SparePart", SparePart);

    SparePart.$inject = ['$resource'];

    function SparePart($resource) {
        return $resource('http://localhost:8080' + '/api/spare_part/:id', {
            id: '@id'
        },{
            'findSparePartByName' : {
                method: 'GET',
                url: 'api/spare_part/findByName',
                isArray: true
            }
        });
    }
})();
