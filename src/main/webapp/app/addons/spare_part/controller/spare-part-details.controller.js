(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('SparePartDetailsController', SparePartDetailsController);

    SparePartDetailsController.$inject = ['SparePart', '$scope', '$stateParams', '$state', 'MachineType'];

    function SparePartDetailsController (SparePart, $scope, $stateParams, $state, MachineType) {
        $scope.entity = {};

        if(angular.isDefined($stateParams.id) && $stateParams.id !== "")
            SparePart.get({id: $stateParams.id}).$promise.then(function(data){
                $scope.entity = data;
            });

        $scope.save = function(){
            SparePart.save($scope.entity).$promise.then(function(entity){
                $scope.addAlert('success', 'El repuesto ha sido creado/actualizado');
                $scope.entity = entity;
                $state.go('spare-part')
            }, function(){
                $scope.addAlert('danger', 'El repuesto no ha sido creado/actualizado');
            });
        };

        $scope.findMachineTypeByName = function(val) {
            return MachineType.findMachineTypeByName({name: val}).$promise;
        };

        $scope.onSelectMachineType = function ($item) {
            $scope.entity.fkMachineType = $item;
        };

        $scope.return = function(){
            $state.go('spare-part');
        }
    }

})();

