(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('SparePartController', SparePartController);

    SparePartController.$inject = ['SparePart', '$scope', '$state'];

    function SparePartController (SparePart, $scope, $state) {
        SparePart.query().$promise.then(function(data){
            $scope.gridOptions.data  = data;
        });


        $scope.gridOptions = {
            columnDefs: [
                {name: 'id', field: 'id'},
                {name: 'Nombre', field: 'name'},
                {name: 'Tipo de maquina', field: 'fkMachineType.name'},
                {
                    name: 'Edición',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.passObject(row.entity)"   class="btn btn-primary" >' +
                    '   editar' +
                    '</button>'
                },
                {
                    name: 'Borrar',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.delete(row.entity)"   class="btn btn-danger" >' +
                    '   borrar' +
                    '</button>'
                }
            ],
            minRowsToShow: 15,
            enableFiltering: true,
            enableSorting: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };

        $scope.passObject = function(entity){
            $state.go("spare-part-details", {id: entity.id});
        }

        $scope.delete = function(entity){
            SparePart.delete({id: entity.id}).$promise.then(function(){
                $scope.addAlert('success', 'El repuesto se ha eliminado');
                // Esta linea recarga una vez se hace la eliminación
                $state.go("spare-part", null, {reload: true});
            }, function(){
                $scope.addAlert('danger', 'El repuesto no se ha eliminado');
            });
        }

    }
})();
