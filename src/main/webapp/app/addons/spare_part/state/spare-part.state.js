(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('spare-part', {
            parent: "root",
            url: "/app/spare_part",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/spare_part/view/spare-part.html',
                    controller: 'SparePartController'
                }
            }
        }).state('spare-part-details', {
            parent: "root",
            url: "/app/spare-part-details/:id",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/spare_part/view/spare-part-details.html',
                    controller: 'SparePartDetailsController'
                }
            }
        });
    }
})();
