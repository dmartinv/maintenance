(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('report', {
            parent: "root",
            url: "/app/report",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/report/view/report.html',
                    controller: 'ReportController'
                }
            }
        });
    }
})();
