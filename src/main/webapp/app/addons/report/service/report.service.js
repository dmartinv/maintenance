
(function() {
    'use strict';
    var app = angular.module("maintenanceApp");
    app.factory("Report", Report);

    Report.$inject = ['$resource'];

    function Report($resource) {
        /* request */
        return $resource('http://localhost:8080' + '/api/report/:id', {
            id: '@id'
        },{
            'findByMechanic' : {
                method: 'GET',
                url: 'api/maintenance/findByMechanic',
                isArray: true
            }
        });
    }
})();
