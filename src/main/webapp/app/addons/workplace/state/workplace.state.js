(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('workplace', {
            parent: "root",
            url: "/app/workplace",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/workplace/view/workplace.html',
                    controller: 'WorkplaceController'
                }
            }
        }).state('workplace-details', {
            parent: "root",
            url: "/app/workplace-details/:id",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/workplace/view/workplace-details.html',
                    controller: 'WorkplaceDetailsController'
                }
            }
        });
    }
})();
