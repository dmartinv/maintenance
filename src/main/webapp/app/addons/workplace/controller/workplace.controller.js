(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('WorkplaceController', WorkplaceController);

    WorkplaceController.$inject = ['Workplace', '$scope', '$state'];

    function WorkplaceController (Workplace, $scope, $state) {


        Workplace.query().$promise.then(function(data){
            $scope.gridOptions.data  = data;
        });


        $scope.gridOptions = {
            columnDefs: [
                {name: 'id', field: 'id'},
                {name: 'Nombre', field: 'name'},
                {name: 'Descripción', field: 'description'},
                {
                    name: 'Edición',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.passObject(row.entity)"   class="btn btn-primary" >' +
                    '   editar' +
                    '</button>'
                },
                {
                    name: 'Borrar',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.delete(row.entity)"   class="btn btn-danger" >' +
                    '   borrar' +
                    '</button>'
                }
            ],
            minRowsToShow: 15,
            enableFiltering: true,
            enableSorting: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };

        $scope.passObject = function(entity){
            $state.go("workplace-details", {id: entity.id});
        }

        $scope.delete = function(entity){
            Workplace.delete({id: entity.id}).$promise.then(function(){
                $scope.addAlert('success', 'El sitio de trabajo se ha eliminado');
                // Esta linea recarga una vez se hace la eliminación
                $state.go("workplace", null, {reload: true});
            }, function(){
                $scope.addAlert('danger', 'El sitio de trabajo no se ha eliminado');
            });
        }

    }
})();
