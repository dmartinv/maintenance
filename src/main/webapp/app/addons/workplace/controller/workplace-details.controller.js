(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('WorkplaceDetailsController', WorkplaceDetailsController);

    WorkplaceDetailsController.$inject = ['Workplace', '$scope', '$stateParams', '$state'];

    function WorkplaceDetailsController (Workplace, $scope, $stateParams, $state) {
        $scope.entity = {};

        if(angular.isDefined($stateParams.id) && $stateParams.id !== "")
            Workplace.get({id: $stateParams.id}).$promise.then(function(data){
                $scope.entity = data;
            });

        $scope.save = function(){
            Workplace.save($scope.entity).$promise.then(function(entity){
                $scope.addAlert('success', 'El sitio de trabajo ha sido creado/actualizado');
                $scope.entity = entity;
                $state.go('workplace')
            }, function(){
                $scope.addAlert('danger', 'El sitio de trabajo no ha sido creado/actualizado');
            });
        };


        $scope.return = function(){
            $state.go('workplace');
        }
    }

})();

