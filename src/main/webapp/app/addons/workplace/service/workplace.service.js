
(function() {
    'use strict';
    var app = angular.module("maintenanceApp");
    app.factory("Workplace", Workplace);

    Workplace.$inject = ['$resource'];

    function Workplace($resource) {
        return $resource('http://localhost:8080' + '/api/workplace/:id', {
            id: '@id'
        },{
            'findWorkplaceByName' : {
                method: 'GET',
                url: 'api/workplace/findByName',
                isArray: true
            }
        });
    }
})();
