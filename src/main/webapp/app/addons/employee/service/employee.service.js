
(function() {
    'use strict';
    var app = angular.module("maintenanceApp");
    app.factory("Employee", Employee);

    Employee.$inject = ['$resource'];

    function Employee($resource) {
        /* request */
        return $resource('http://localhost:8080' + '/api/employee/:id', {
            id: '@id'
        },{
            'findEmployeeByName' : {
                method: 'GET',
                url: 'api/employee/findByName',
                isArray: true
            },
            'findByJobPosition' : {
                method: 'GET',
                url: 'api/employee/findByJobPosition',
                isArray: true
            }
        });
    }
})();
