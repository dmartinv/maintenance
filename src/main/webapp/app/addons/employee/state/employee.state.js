(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('employee', {
            parent: "root",
            url: "/app/employee",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/employee/view/employee.html',
                    controller: 'EmployeeController'
                }
            }
        }).state('employee-details', {
            parent: "root",
            url: "/app/employee-details/:id",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/employee/view/employee-details.html',
                    controller: 'EmployeeDetailsController'
                }
            }
        });
    }
})();
