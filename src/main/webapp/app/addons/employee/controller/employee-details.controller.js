(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('EmployeeDetailsController', EmployeeDetailsController);

    EmployeeDetailsController.$inject = ['Employee', '$scope', '$stateParams', 'Department', '$state'];

    function EmployeeDetailsController (Employee, $scope, $stateParams, Department, $state) {
        $scope.entity = {};

        if(angular.isDefined($stateParams.id) && $stateParams.id !== "")
            Employee.get({id: $stateParams.id}).$promise.then(function(data){
                $scope.entity = data;
            });

        $scope.save = function(){
            $scope.entity.jobPosition = 'Operario'
            Employee.save($scope.entity).$promise.then(function(entity){
                $scope.addAlert('success', 'El empleado ha sido creado/actualizado');
                $scope.entity = entity;
                $state.go('employee')
            }, function(){
                $scope.addAlert('danger', 'El empleado no ha sido creado/actualizado');
            });
        };

        /*Ejecuta la busqueda en la web de los departamentos por nombre*/
        $scope.findDepartmentByName = function(val) {
            return Department.findDepartmentByName({name: val}).$promise;
        };

        /*Al seleccionar de la busqueda asigna el departamento a la entidad*/
        $scope.onSelectDepartment = function ($item) {
            $scope.entity.fkDepartment = $item;
        };

        $scope.return = function(){
            $state.go('employee');
        }
    }

})();
