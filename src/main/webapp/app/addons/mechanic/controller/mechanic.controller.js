(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MechanicController', MechanicController);

    MechanicController.$inject = ['Mechanic', '$scope', '$state'];

    function MechanicController (Mechanic, $scope, $state) {
        Mechanic.findByJobPosition({jobPosition: "Mecánico"}).$promise.then(function(data){
            $scope.gridOptions.data  = data;
        });


        $scope.gridOptions = {
            columnDefs: [
                {name: 'id', field: 'id'},
                {name: 'Nombre', field: 'name'},
                {name: 'Apellido', field: 'lastName'},
                {name: 'Identificación', field: 'identification'},
                {name: 'Género', field: 'gender'},
                {name: 'Salario', field: 'salary'},

                {
                    name: 'Edición',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.passObject(row.entity)"   class="btn btn-primary" >' +
                    '   editar' +
                    '</button>'
                },
                {
                    name: 'Borrar',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.delete(row.entity)"   class="btn btn-danger" >' +
                    '   borrar' +
                    '</button>'
                }
            ],
            minRowsToShow: 15,
            enableFiltering: true,
            enableSorting: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };

        $scope.passObject = function(entity){
            $state.go("mechanic-details", {id: entity.id});
        }

        $scope.delete = function(entity){
            Mechanic.delete({id: entity.id}).$promise.then(function(){
                $scope.addAlert('success', 'El empleado se ha eliminado');
                // Esta linea recarga una vez se hace la eliminación
                $state.go("mechanic", null, {reload: true});
            }, function(){
                $scope.addAlert('danger', 'El empleado no se ha eliminado');
            });
        }

    }
})();
