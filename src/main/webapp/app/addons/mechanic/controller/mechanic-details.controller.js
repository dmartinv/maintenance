(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MechanicDetailsController', MechanicDetailsController);

    MechanicDetailsController.$inject = ['Mechanic', '$scope', '$stateParams', 'Department', '$state'];

    function MechanicDetailsController (Mechanic, $scope, $stateParams, Department, $state) {
        $scope.entity = {};

        if(angular.isDefined($stateParams.id) && $stateParams.id !== "")
            Mechanic.get({id: $stateParams.id}).$promise.then(function(data){
                $scope.entity = data;
            });

        $scope.save = function(){
            $scope.entity.jobPosition = 'Mecánico'
            Mechanic.save($scope.entity).$promise.then(function(entity){
                $scope.addAlert('success', 'El empleado ha sido creado/actualizado');
                $scope.entity = entity;
                $state.go('mechanic')
            }, function(){
                $scope.addAlert('danger', 'El empleado no ha sido creado/actualizado');
            });
        };

        /*Ejecuta la busqueda en la web de los departamentos por nombre*/
        $scope.findDepartmentByName = function(val) {
            return Department.findDepartmentByName({name: val}).$promise;
        };

        /*Al seleccionar de la busqueda asigna el departamento a la entidad*/
        $scope.onSelectDepartment = function ($item) {
            $scope.entity.fkDepartment = $item;
        };

        $scope.return = function(){
            $state.go('mechanic');
        }
    }

})();
