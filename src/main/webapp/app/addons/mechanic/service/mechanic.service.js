
(function() {
    'use strict';
    var app = angular.module("maintenanceApp");
    app.factory("Mechanic", Mechanic);

    Mechanic.$inject = ['$resource'];

    function Mechanic($resource) {
        /* request */
        return $resource('http://localhost:8080' + '/api/employee/:id', {
            id: '@id'
        },{
            'findMechanicByName' : {
                method: 'GET',
                url: 'api/employee/findByName',
                isArray: true
            },
            'findByJobPosition' : {
                method: 'GET',
                url: 'api/employee/findByJobPosition',
                isArray: true
            },
            'findByNameAndJobPosition' : {
                method: 'GET',
                url: 'api/employee/findByNameAndJobPosition',
                isArray: true
            }
        });
    }
})();
