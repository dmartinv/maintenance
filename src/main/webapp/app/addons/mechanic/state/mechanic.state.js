(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('mechanic', {
            parent: "root",
            url: "/app/mechanic",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/mechanic/view/mechanic.html',
                    controller: 'MechanicController'
                }
            }
        }).state('mechanic-details', {
            parent: "root",
            url: "/app/mechanic-details/:id",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/mechanic/view/mechanic-details.html',
                    controller: 'MechanicDetailsController'
                }
            }
        });
    }
})();
