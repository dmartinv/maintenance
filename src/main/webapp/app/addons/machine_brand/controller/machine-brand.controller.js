(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MachineBrandController', MachineBrandController);

    MachineBrandController.$inject = ['MachineBrand', '$scope', '$state'];

    function MachineBrandController (MachineBrand, $scope, $state) {
        MachineBrand.query().$promise.then(function(data){
            $scope.gridOptions.data  = data;
        });


        $scope.gridOptions = {
            columnDefs: [
                {name: 'id', field: 'id'},
                {name: 'Nombre', field: 'name'},
                {
                    name: 'Edición',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.passObject(row.entity)"   class="btn btn-primary" >' +
                    '   editar' +
                    '</button>'
                },
                {
                    name: 'Borrar',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.delete(row.entity)"   class="btn btn-danger" >' +
                    '   borrar' +
                    '</button>'
                }
            ],
            minRowsToShow: 15,
            enableFiltering: true,
            enableSorting: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };

        $scope.passObject = function(entity){
            $state.go("machine-brand-details", {id: entity.id});
        }

        $scope.delete = function(entity){
            MachineBrand.delete({id: entity.id}).$promise.then(function(){
                $scope.addAlert('success', 'La marca de máquina se ha eliminado');
                // Esta linea recarga una vez se hace la eliminación
                $state.go("machine-brand", null, {reload: true});
            }, function(){
                $scope.addAlert('danger', 'La marca de máquina no se ha eliminado');
            });
        }

    }
})();
