(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MachineBrandDetailsController', MachineBrandDetailsController);

    MachineBrandDetailsController.$inject = ['MachineBrand', '$scope', '$stateParams', '$state'];

    function MachineBrandDetailsController (MachineBrand, $scope, $stateParams, $state) {
        $scope.entity = {};

        if(angular.isDefined($stateParams.id) && $stateParams.id !== "")
            MachineBrand.get({id: $stateParams.id}).$promise.then(function(data){
                $scope.entity = data;
            });

        $scope.save = function(){
            MachineBrand.save($scope.entity).$promise.then(function(entity){
                $scope.addAlert('success', 'La marca de máquina ha sido creada/actualizada');
                $scope.entity = entity;
                $state.go('machine-brand');
            }, function(){
                $scope.addAlert('danger', 'La marca de máquina no ha sido creada/actualizada');
            });
        };


        $scope.return = function(){
            $state.go('machine-brand');
        }
    }

})();