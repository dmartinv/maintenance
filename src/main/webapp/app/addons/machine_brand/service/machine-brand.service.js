
(function() {
    'use strict';
    var app = angular.module("maintenanceApp");
    app.factory("MachineBrand", MachineBrand);

    MachineBrand.$inject = ['$resource'];

    function MachineBrand($resource) {
        /* request */
        return $resource('http://localhost:8080' + '/api/machine_brand/:id', {
            id: '@id'
        },{
            'findMachineBrandByName' : {
                method: 'GET',
                url: 'api/machine_brand/findByName',
                isArray: true
            }
        });
    }
})();
