(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('machine-brand', {
            parent: "root",
            url: "/app/machine_brand",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/machine_brand/view/machine-brand.html',
                    controller: 'MachineBrandController'
                }
            }
        }).state('machine-brand-details', {
            parent: "root",
            url: "/app/machine-brand-details/:id",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/machine_brand/view/machine-brand-details.html',
                    controller: 'MachineBrandDetailsController'
                }
            }
        });
    }
})();
