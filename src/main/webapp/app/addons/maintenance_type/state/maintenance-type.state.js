(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('maintenance-type', {
            parent: "root",
            url: "/app/maintenance_type",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/maintenance_type/view/maintenance-type.html',
                    controller: 'MaintenanceTypeController'
                }
            }
        }).state('maintenance-type-details', {
            parent: "root",
            url: "/app/maintenance-type-details/:id",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/maintenance_type/view/maintenance-type-details.html',
                    controller: 'MaintenanceTypeDetailsController'
                }
            }
        });
    }
})();
