(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MaintenanceTypeController', MaintenanceTypeController);

    MaintenanceTypeController.$inject = ['MaintenanceType', '$scope', '$state'];

    function MaintenanceTypeController (MaintenanceType, $scope, $state) {
        MaintenanceType.query().$promise.then(function(data){
            $scope.gridOptions.data  = data;
        });


        $scope.gridOptions = {
            columnDefs: [
                {name: 'id', field: 'id'},
                {name: 'Nombre', field: 'name'},
                {name: 'Descripción', field: 'description'},
                {name: 'Horas estándar', field: 'standardHours'},
                {
                    name: 'Edición',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.passObject(row.entity)"   class="btn btn-primary" >' +
                    '   editar' +
                    '</button>'
                },
                {
                    name: 'Borrar',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.delete(row.entity)"   class="btn btn-danger" >' +
                    '   borrar' +
                    '</button>'
                }
            ],
            minRowsToShow: 15,
            enableFiltering: true,
            enableSorting: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };

        $scope.passObject = function(entity){
            $state.go("maintenance-type-details", {id: entity.id});
        }

        $scope.delete = function(entity){
            MaintenanceType.delete({id: entity.id}).$promise.then(function(){
                $scope.addAlert('success', 'El tipo de mantenimiento se ha eliminado');
                // Esta linea recarga una vez se hace la eliminación
                $state.go("maintenance-type", null, {reload: true});
            }, function(){
                $scope.addAlert('danger', 'El tipo de mantenimiento no se ha eliminado');
            });
        }

    }
})();
