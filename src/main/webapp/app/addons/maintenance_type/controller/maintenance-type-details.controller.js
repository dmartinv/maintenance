(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MaintenanceTypeDetailsController', MaintenanceTypeDetailsController);

    MaintenanceTypeDetailsController.$inject = ['MaintenanceType', '$scope', '$stateParams', '$state'];

    function MaintenanceTypeDetailsController (MaintenanceType, $scope, $stateParams, $state) {
        $scope.entity = {};

        if(angular.isDefined($stateParams.id) && $stateParams.id !== "")
            MaintenanceType.get({id: $stateParams.id}).$promise.then(function(data){
                $scope.entity = data;
            });

        $scope.save = function(){
            MaintenanceType.save($scope.entity).$promise.then(function(entity){
                $scope.addAlert('success', 'El tipo de mantenimiento ha sido creado/actualizado');
                $scope.entity = entity;
                $state.go('maintenance-type');
            }, function(){
                $scope.addAlert('danger', 'El tipo de mantenimiento no ha sido creado/actualizado');
            });
        };


        $scope.return = function(){
            $state.go('maintenance-type');
        }
    }

})();

