
(function() {
    'use strict';
    var app = angular.module("maintenanceApp");
    app.factory("MaintenanceType", MaintenanceType);

    MaintenanceType.$inject = ['$resource'];

    function MaintenanceType($resource) {
        /* request */
        return $resource('http://localhost:8080' + '/api/maintenance_type/:id', {
            id: '@id'
        },{
            'findMaintenanceTypeByName' : {
                method: 'GET',
                url: 'api/maintenance_type/findByName',
                isArray: true
            }
        });
    }
})();
