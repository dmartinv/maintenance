(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('maintenance', {
            parent: "root",
            url: "/app/maintenance",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/maintenance/view/maintenance.html',
                    controller: 'MaintenanceController'
                }
            }
        }).state('maintenance-details', {
            parent: "root",
            url: "/app/maintenance-details/:id",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/maintenance/view/maintenance-details.html',
                    controller: 'MaintenanceDetailsController'
                }
            }
        }).state('maintenance-task-modal', {
            parent: 'maintenance-details',
            url: '/app/maintenance-details/:id/modal-new/:maintenanceTaskId',
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                var stateParamsId = $stateParams.operationId;
                $uibModal.open({
                    templateUrl: 'app/addons/maintenance_task/view/maintenance-task-modal.html',
                    controller: 'MaintenanceTaskModalController',
                    size: 'lg',
                    resolve: {
                        entity: ['$stateParams', 'Maintenance', function($stateParams, Maintenance) {
                            if(isNull($stateParams.operationId)){
                                return {
                                    id: null
                                };
                            }
                            else {
                                return Maintenance.get({id: stateParamsId});
                            }
                        }]
                    }
                }).result.then(function() {
                    $state.go('maintenance-details', {id: $stateParams.id}, { reload: 'maintenance-details' });
                }, function() {
                    $state.go('maintenance-details', {id: $stateParams.id});
                });
            }]
        });
    }
})();
