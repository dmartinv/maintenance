
(function() {
    'use strict';
    var app = angular.module("maintenanceApp");
    app.factory("Maintenance", Maintenance);

    Maintenance.$inject = ['$resource'];

    function Maintenance($resource) {
        /* request */
        return $resource('http://localhost:8080' + '/api/maintenance/:id', {
            id: '@id'
        },{
            'findMaintenanceByName' : {
                method: 'GET',
                url: 'api/maintenance/findByName',
                isArray: true
            }
        });
    }
})();
