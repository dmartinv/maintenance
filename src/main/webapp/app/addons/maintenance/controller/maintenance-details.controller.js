(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MaintenanceDetailsController', MaintenanceDetailsController);

    MaintenanceDetailsController.$inject = ['Maintenance', '$scope', '$stateParams',
        'Mechanic', 'MachineFailure', 'MaintenanceType','Machine', '$state', 'MaintenanceTask'];

    function MaintenanceDetailsController (Maintenance, $scope, $stateParams,
                                           Mechanic, MachineFailure, MaintenanceType, Machine, $state,
                                           MaintenanceTask ) {
        $scope.entity = {};

        if(angular.isDefined($stateParams.id) && $stateParams.id !== "")
            Maintenance.get({id: $stateParams.id}).$promise.then(function(data){
                $scope.entity = data;
                $scope.searchMaintenanceTask();
            });

        $scope.save = function(){
            Maintenance.save($scope.entity).$promise.then(function(entity){
                $scope.addAlert('success', 'El registro de mantenimiento ha sido creado/actualizado');
                $scope.entity = entity;
            }, function(){
                $scope.addAlert('danger', 'El registro de mantenimiento no ha sido creado/actualizado');
            });
        };

        $scope.findMachineByName = function(val) {
            return Machine.findMachineByName({name: val}).$promise;
        };

        $scope.onSelectMachine = function ($item) {
            $scope.entity.fkMachine = $item;
        };

        /*Ejecuta la busqueda en la web de los empleados por nombre*/
        $scope.findEmployeeByName = function(val) {
            return Mechanic.findByNameAndJobPosition({jobPosition: 'Mecánico', name: val}).$promise;
        };

        /*Al seleccionar de la busqueda asigna el empleado a la entidad*/
        $scope.onSelectEmployee = function ($item) {
            $scope.entity.fkEmployee = $item;
        };

        $scope.findMachineFailureByName = function(val) {
            return MachineFailure.findMachineFailureByName({name: val}).$promise;
        };

        $scope.onSelectMachineFailure = function ($item) {
            $scope.entity.fkMachineFailure = $item;
        };

        $scope.findMaintenanceTypeByName = function(val) {
            return MaintenanceType.findMaintenanceTypeByName({name: val}).$promise;
        };

        $scope.onSelectMaintenanceType = function ($item) {
            $scope.entity.fkMaintenanceType = $item;
        };

        $scope.return = function(){
            $state.go('maintenance');
        }

        $scope.gridOptions = {
            columnDefs: [
                {name: 'id', field: 'id'},
                {name: 'Nombre', field: 'name'},
                {name: 'Descripción', field: 'description'},
                {
                    name: 'Edición',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.passObject(row.entity)"   class="btn btn-primary" >' +
                    '   editar' +
                    '</button>'
                },
                {
                    name: 'Borrar',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.delete(row.entity)"   class="btn btn-danger" >' +
                    '   borrar' +
                    '</button>'
                }
            ],
            minRowsToShow: 15,
            enableFiltering: true,
            enableSorting: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };

        $scope.passObject = function(maintenance){
            $state.go("maintenance-task-modal", {maintenanceTaskId: maintenance.id, id: $scope.entity.id});
        };

        $scope.searchMaintenanceTask = function(){
            MaintenanceTask.findByFkMaintenance({fkMaintenance: $scope.entity.id}).$promise.then(function (operations) {
                $scope.gridOptions.data =  operations;
            });
        };

        $scope.delete = function(entity){
            MaintenanceTask.delete({id: entity.id}).$promise.then(function(){
                $scope.addAlert('success', 'El registro de la tarea del mantenimiento se ha eliminado');
                // Esta linea recarga una vez se hace la eliminación
                $state.go("maintenance-details", null, {reload: true});
            }, function(){
                $scope.addAlert('danger', 'El registro no se ha eliminado');
            });
        }
    }
})();
