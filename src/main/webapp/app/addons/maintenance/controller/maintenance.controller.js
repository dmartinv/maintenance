(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MaintenanceController', MaintenanceController);

    MaintenanceController.$inject = ['Maintenance', '$scope', '$state'];

    function MaintenanceController (Maintenance, $scope, $state) {
        Maintenance.query().$promise.then(function(data){
            $scope.gridOptions.data  = data;
        });


        $scope.gridOptions = {
            columnDefs: [
                {name: 'id', field: 'id'},
                {name: 'Nombre', field: 'name'},
                {name: 'Prioridad', field: 'priority'},
                {name: 'Estado', field: 'state'},
                {name: 'Horas estándar', field: 'standardHours'},
                {name: 'Fecha de vencimiento', field: 'dueDate'},
                {name: 'Fecha de inicio', field: 'startDate'},
                {name: 'Description', field: 'description'},
                {
                    name: 'Edición',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.passObject(row.entity)"   class="btn btn-primary" >' +
                    '   editar' +
                    '</button>'
                },
                {
                    name: 'Borrar',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.delete(row.entity)"   class="btn btn-danger" >' +
                    '   borrar' +
                    '</button>'
                }
            ],
            minRowsToShow: 15,
            enableFiltering: true,
            enableSorting: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };

        $scope.passObject = function(entity){
            $state.go("maintenance-details", {id: entity.id});
        }

        $scope.delete = function(entity){
            Maintenance.delete({id: entity.id}).$promise.then(function(){
                $scope.addAlert('success', 'El registro de mantenimiento se ha eliminado');
                // Esta linea recarga una vez se hace la eliminación
                $state.go("maintenance", null, {reload: true});
            }, function(){
                $scope.addAlert('danger', 'El registro no se ha eliminado');
            });
        }

    }
})();
