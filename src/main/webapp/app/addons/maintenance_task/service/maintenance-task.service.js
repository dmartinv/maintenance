
(function() {
    'use strict';
    var app = angular.module("maintenanceApp");
    app.factory("MaintenanceTask", MaintenanceTask);

    MaintenanceTask.$inject = ['$resource'];

    function MaintenanceTask($resource) {
        /* request */
        return $resource('http://localhost:8080' + '/api/maintenance_task/:id', {
            id: '@id'
        },{
            'findMaintenanceTaskByName' : {
                method: 'GET',
                url: 'api/maintenance_task/findByName',
                isArray: true
            },
            'findByFkMaintenance' : {
                method: 'GET',
                url: 'api/maintenance_task/findByFkMaintenance',
                isArray: true
            }
        });
    }
})();
