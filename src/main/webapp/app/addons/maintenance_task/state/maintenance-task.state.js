(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('maintenance-task', {
            parent: "root",
            url: "/app/maintenance_task",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/maintenance_task/view/maintenance-task.html',
                    controller: 'MaintenanceTaskController'
                }
            }
        }).state('maintenance-task-details', {
            parent: "root",
            url: "/app/maintenance-task-details/:id",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/maintenance_task/view/maintenance-task-details.html',
                    controller: 'MaintenanceTaskDetailsController'
                }
            }
        });
    }
})();
