(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MaintenanceTaskDetailsController', MaintenanceTaskDetailsController);

    MaintenanceTaskDetailsController.$inject = ['MaintenanceTask', '$scope', '$stateParams', '$state'];

    function MaintenanceTaskDetailsController (MaintenanceTask, $scope, $stateParams, $state) {
        $scope.entity = {};

        if(angular.isDefined($stateParams.id) && $stateParams.id !== "")
            MaintenanceTask.get({id: $stateParams.id}).$promise.then(function(data){
                $scope.entity = data;
            });

        $scope.save = function(){
            MaintenanceTask.save($scope.entity).$promise.then(function(entity){
                $scope.addAlert('success', 'La actividad de mantenimiento ha sido creada/actualizada');
                $scope.entity = entity;
                $state.go('maintenance-task');
            }, function(){
                $scope.addAlert('danger', 'La actividad de mantenimiento no ha sido creada/actualizada');
            });
        }

        /*Ejecuta la busqueda en la web de las actividades de mantenimiento por id*/
        $scope.findMaintenanceTaskByName = function(val) {
            return Department.findMaintenanceTaskByName({name: val}).$promise;
        };

        /*Al seleccionar de la busqueda asigna la actividad de mantenimiento a la entidad*/
        $scope.onSelectMaintenance = function ($item) {
            $scope.entity.fkMaintenance = $item;
        };

        $scope.return = function(){
            $state.go('maintenance-task');
        }
        
        
    }
})();
