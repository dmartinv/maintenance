(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MaintenanceTaskModalController', MaintenanceTaskModalController);

    MaintenanceTaskModalController.$inject = ['MaintenanceTask', '$scope', '$stateParams', '$state', '$uibModalInstance'];

    function MaintenanceTaskModalController (MaintenanceTask, $scope, $stateParams, $state, $uibModalInstance) {
        $scope.entity = {};
        $scope.entity.fkMaintenance = {};
        $scope.entity.fkMaintenance.id = $stateParams.id;
        if(angular.isDefined($stateParams.maintenanceTaskId) && $stateParams.maintenanceTaskId !== "")
            MaintenanceTask.get({id: $stateParams.maintenanceTaskId}).$promise.then(function(data){
                $scope.entity = data;
            });

        $scope.save = function(){
            MaintenanceTask.save($scope.entity).$promise.then(function(entity){
                $scope.addAlert('success', 'La actividad de mantenimiento ha sido creada/actualizada');
                $scope.entity = entity;
                $uibModalInstance.close();
            }, function(){
                $scope.addAlert('danger', 'La actividad de mantenimiento no ha sido creada/actualizada');
            });
        }

        /*Ejecuta la busqueda en la web de las actividades de mantenimiento por id*/
        $scope.findMaintenanceByName = function(val) {
            return Department.findMaintenanceTaskByName({name: val}).$promise;
        };

        /*Al seleccionar de la busqueda asigna la actividad de mantenimiento a la entidad*/
        $scope.onSelectMaintenance = function ($item) {
            $scope.entity.fkMaintenance = $item;
        };

        $scope.return = function(){
            $state.go('maintenance-details');
            $uibModalInstance.close();
        }
    }
})();
