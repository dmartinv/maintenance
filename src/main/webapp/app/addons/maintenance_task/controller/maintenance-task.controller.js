(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MaintenanceTaskController', MaintenanceTaskController);

    MaintenanceTaskController.$inject = ['MaintenanceTask', '$scope', '$state'];

    function MaintenanceTaskController (MaintenanceTask, $scope, $state) {
        MaintenanceTask.query().$promise.then(function(data){
            $scope.gridOptions.data  = data;
        });


        $scope.gridOptions = {
            columnDefs: [
                {name: 'id', field: 'id'},
                {name: 'Nombre', field: 'name'},
                {name: 'Descripción', field: 'description'},
                {name: 'Mantenimiento', field: 'fkMaintenance.name'},
                {
                    name: 'Edición',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.passObject(row.entity)"   class="btn btn-primary" >' +
                    '   editar' +
                    '</button>'
                },
                {
                    name: 'Borrar',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.delete(row.entity)"   class="btn btn-danger" >' +
                    '   borrar' +
                    '</button>'
                }
            ],
            minRowsToShow: 15,
            enableFiltering: true,
            enableSorting: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };

        $scope.passObject = function(entity){
            $state.go("maintenance-task-details", {id: entity.id});
        }

        $scope.delete = function(entity){
            MaintenanceTask.delete({id: entity.id}).$promise.then(function(){
                $scope.addAlert('success', 'La actividad de mantenimiento se ha eliminado');
                // Esta linea recarga una vez se hace la eliminación
                $state.go("maintenance-task", null, {reload: true});
            }, function(){
                $scope.addAlert('danger', 'La actividad de mantenimiento no se ha eliminado');
            });
        }

    }
})();
