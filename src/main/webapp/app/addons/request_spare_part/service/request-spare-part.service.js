
(function() {
    'use strict';
    var app = angular.module("maintenanceApp");
    app.factory("RequestSparePart", RequestSparePart);

    RequestSparePart.$inject = ['$resource'];

    function RequestSparePart($resource) {
        return $resource('http://localhost:8080' + '/api/request_spare_part/:id', {
            id: '@id'
        });
    }
})();
