(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('request-spare-part', {
            parent: "root",
            url: "/app/request_spare_part",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/request_spare_part/view/request-spare-part.html',
                    controller: 'RequestSparePartController'
                }
            }
        }).state('request-spare-part-details', {
            parent: "root",
            url: "/app/request-spare-part-details/:id",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/request_spare_part/view/request-spare-part-details.html',
                    controller: 'RequestSparePartDetailsController'
                }
            }
        });
    }
})();
