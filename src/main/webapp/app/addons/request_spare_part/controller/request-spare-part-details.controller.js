(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('RequestSparePartDetailsController', RequestSparePartDetailsController);

    RequestSparePartDetailsController.$inject = ['RequestSparePart', '$scope',
        '$stateParams', 'SparePart', 'Machine', '$state'];

    function RequestSparePartDetailsController (RequestSparePart, $scope,
                                                $stateParams, SparePart, Machine, $state) {
        $scope.entity = {};

        if(angular.isDefined($stateParams.id) && $stateParams.id !== "")
            RequestSparePart.get({id: $stateParams.id}).$promise.then(function(data){
                $scope.entity = data;
            });

        $scope.save = function(){
            RequestSparePart.save($scope.entity).$promise.then(function(entity){
                $scope.addAlert('success', 'El requerimiento de repuesto ha sido creado/actualizado');
                $scope.entity = entity;
                $state.go('request-spare-part')
            }, function(){
                $scope.addAlert('danger', 'El requerimiento de repuesto no ha sido creado/actualizado');
            });
        }


        $scope.findSparePartByName = function(val) {
            return SparePart.findSparePartByName({name: val}).$promise;
        };


        $scope.onSelectSparePart = function ($item) {
            $scope.entity.fkSparePart = $item;
        };

        $scope.findMachineByName = function(val) {
            return Machine.findMachineByName({name: val}).$promise;
        };


        $scope.onSelectMachine = function ($item) {
            $scope.entity.fkMachine = $item;
        };

        $scope.return = function(){
            $state.go('request-spare-part');
        }
    }
})();

