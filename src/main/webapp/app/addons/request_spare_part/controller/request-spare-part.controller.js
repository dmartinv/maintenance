(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('RequestSparePartController', RequestSparePartController);

    RequestSparePartController.$inject = ['RequestSparePart', '$scope', '$state'];

    function RequestSparePartController (RequestSparePart, $scope, $state) {
        RequestSparePart.query().$promise.then(function(data){
            $scope.gridOptions.data  = data;
        });


        $scope.gridOptions = {
            columnDefs: [
                {name: 'id', field: 'id'},
                {name: 'Fecha Solicitud', field: 'requestDate'},
                {name: 'Fecha Entrega', field: 'deliveryDate'},
                {name: 'Unidades', field: 'units'},
                {
                    name: 'Edicion',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.passObject(row.entity)"  class="btn btn-icon " >' +
                    '   editar' +
                    '</button>'
                },
                {
                    name: 'Borrar',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.delete(row.entity)"  class="btn btn-icon " >' +
                    '   borrar' +
                    '</button>'
                }
            ],
            minRowsToShow: 15,
            enableFiltering: true,
            enableSorting: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };

        $scope.passObject = function(entity){
            $state.go("request-spare-part-details", {id: entity.id});
        }

        $scope.delete = function(entity){
            RequestSparePart.delete({id: entity.id}).$promise.then(function(){
                $scope.addAlert('success', 'El requerimiento de repuesto se ha eliminado');
                // Esta linea recarga una vez se hace la eliminación
                $state.go("request-spare-part", null, {reload: true});
            }, function(){
                $scope.addAlert('danger', 'El requerimiento de repuesto no se ha eliminado');
            });
        }

    }
})();
