(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('machine-category', {
            parent: "root",
            url: "/app/machine_category",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/machine_category/view/machine-category.html',
                    controller: 'MachineCategoryController'
                }
            }
        }).state('machine-category-details', {
            parent: "root",
            url: "/app/machine-category-details/:id",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/machine_category/view/machine-category-details.html',
                    controller: 'MachineCategoryDetailsController'
                }
            }
        });
    }
})();
