
(function() {
    'use strict';
    var app = angular.module("maintenanceApp");
    app.factory("MachineCategory", MachineCategory);

    MachineCategory.$inject = ['$resource'];

    function MachineCategory($resource) {
        /* request */
        return $resource('http://localhost:8080' + '/api/machine_category/:id', {
            id: '@id'
        },{
            'findMachineCategoryByName' : {
                method: 'GET',
                url: 'api/machine_category/findByName',
                isArray: true
            }
        });
    }
})();
