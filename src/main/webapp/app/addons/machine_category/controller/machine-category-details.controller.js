(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MachineCategoryDetailsController', MachineCategoryDetailsController);

    MachineCategoryDetailsController.$inject = ['MachineCategory', '$scope', '$stateParams', '$state'];

    function MachineCategoryDetailsController (MachineCategory, $scope, $stateParams, $state) {
        $scope.entity = {};

        if(angular.isDefined($stateParams.id) && $stateParams.id !== "")
            MachineCategory.get({id: $stateParams.id}).$promise.then(function(data){
                $scope.entity = data;
            });

        $scope.save = function(){
            MachineCategory.save($scope.entity).$promise.then(function(entity){
                $scope.addAlert('success', 'La categoría de máquina ha sido creado/actualizado');
                $scope.entity = entity;
                $state.go('machine-category');
            }, function(){
                $scope.addAlert('danger', 'La categoría de máquina no ha sido creado/actualizado');
            });
        };


        $scope.return = function(){
            $state.go('machine-category');
        }
    }

})();