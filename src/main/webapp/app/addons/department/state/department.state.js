(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('department', {
            parent: "root",
            url: "/app/department",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/department/view/department.html',
                    controller: 'DepartmentController'
                }
            }
        }).state('department-details', {
            parent: "root",
            url: "/app/department-details/:id",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/department/view/department-details.html',
                    controller: 'DepartmentDetailsController'
                }
            }
        });
    }
})();
