
(function() {
    'use strict';
    var app = angular.module("maintenanceApp");
    app.factory("Department", Department);

    Department.$inject = ['$resource'];

    function Department($resource) {
        /* request */
        return $resource('http://localhost:8080' + '/api/department/:id', {
            id: '@id'
        },{
            'findDepartmentByName' : {
                method: 'GET',
                url: 'api/department/findByName',
                isArray: true
            }
        });
    }
})();
