(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('DepartmentDetailsController', DepartmentDetailsController);

    DepartmentDetailsController.$inject = ['Department', '$scope', '$stateParams', '$state'];

    function DepartmentDetailsController (Department, $scope, $stateParams, $state) {
        $scope.entity = {};
        if(angular.isDefined($stateParams.id) && $stateParams.id !== "")
            Department.get({id: $stateParams.id}).$promise.then(function(data){
                $scope.entity = data;
            });

        $scope.save = function(){
            // Una vez se guarda, se agrega el alert
            Department.save($scope.entity).$promise.then(function(entity){
                $scope.addAlert('success', 'El departamento ha sido creado/actualizado');
                $scope.entity = entity;
                $state.go('department');
            }, function(){
                $scope.addAlert('danger', 'El departamento no ha sido creado/actualizado');
            });
        }

        $scope.return = function(){
            $state.go('department');
        }
    }
})();
