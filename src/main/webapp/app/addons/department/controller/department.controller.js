(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('DepartmentController', DepartmentController);

    DepartmentController.$inject = ['Department', '$scope', '$state'];

    function DepartmentController (Department, $scope, $state) {
        Department.query().$promise.then(function(data){
            $scope.gridOptions.data  = data;
        });


        $scope.gridOptions = {
            columnDefs: [
                {name: 'id', field: 'id'},
                {name: 'Nombre', field: 'name'},
                {
                    name: 'Edición',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.passObject(row.entity)"   class="btn btn-primary" >' +
                    '   editar' +
                    '</button>'
                },
                {
                    name: 'Borrar',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.delete(row.entity)"   class="btn btn-danger" >' +
                    '   borrar' +
                    '</button>'
                }
            ],
            minRowsToShow: 15,
            enableFiltering: true,
            enableSorting: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };

        $scope.passObject = function(entity){
            $state.go("department-details", {id: entity.id});
        };

        $scope.delete = function(entity){
            Department.delete({id: entity.id}).$promise.then(function(){
                $scope.addAlert('success', 'El departamento se ha eliminado');
                // Esta linea recarga una vez se hace la eliminación
                $state.go("department", null, {reload: true});
            }, function(){
                $scope.addAlert('danger', 'El departamento no se ha eliminado');
            });
        }

    }
})();
