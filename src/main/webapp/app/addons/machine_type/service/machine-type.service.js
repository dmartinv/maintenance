
(function() {
    'use strict';
    var app = angular.module("maintenanceApp");
    app.factory("MachineType", MachineType);

    MachineType.$inject = ['$resource'];

    function MachineType($resource) {
        /* request */
        return $resource('http://localhost:8080' + '/api/machine_type/:id', {
            id: '@id'
        },{
            'findMachineTypeByName' : {
                method: 'GET',
                url: 'api/machine_type/findByName',
                isArray: true
            }
        });
    }
})();
