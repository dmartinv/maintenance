(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MachineTypeController', MachineTypeController);

    MachineTypeController.$inject = ['MachineType', '$scope', '$state'];

    function MachineTypeController (MachineType, $scope, $state) {
        MachineType.query().$promise.then(function(data){
            $scope.gridOptions.data  = data;
        });


        $scope.gridOptions = {
            columnDefs: [
                {name: 'id', field: 'id'},
                {name: 'Nombre', field: 'name'},
                {name: 'Descripción', field: 'description'},
                {
                    name: 'Edición',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.passObject(row.entity)"   class="btn btn-primary" >' +
                    '   editar' +
                    '</button>'
                },
                {
                    name: 'Borrar',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.delete(row.entity)"   class="btn btn-danger" >' +
                    '   borrar' +
                    '</button>'
                }
            ],
            minRowsToShow: 15,
            enableFiltering: true,
            enableSorting: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };

        $scope.passObject = function(entity){
            $state.go("machine-type-details", {id: entity.id});
        }

        $scope.delete = function(entity){
            MachineType.delete({id: entity.id}).$promise.then(function(){
                $scope.addAlert('success', 'El tipo de máquina se ha eliminado');
                // Esta linea recarga una vez se hace la eliminación
                $state.go("machine-type", null, {reload: true});
            }, function(){
                $scope.addAlert('danger', 'El tipo de máquina no se ha eliminado');
            });
        }

    }
})();
