(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MachineTypeDetailsController', MachineTypeDetailsController);

    MachineTypeDetailsController.$inject = ['MachineType', '$scope', '$stateParams', '$state'];

    function MachineTypeDetailsController (MachineType, $scope, $stateParams, $state) {
        $scope.entity = {};

        if(angular.isDefined($stateParams.id) && $stateParams.id !== "")
            MachineType.get({id: $stateParams.id}).$promise.then(function(data){
                $scope.entity = data;
            });

        $scope.save = function(){
            MachineType.save($scope.entity).$promise.then(function(entity){
                $scope.addAlert('success', 'El tipo de máquina ha sido creado/actualizado');
                $scope.entity = entity;
                $state.go('machine-type');
            }, function(){
                $scope.addAlert('danger', 'El tipo de máquina no ha sido creado/actualizado');
            });
        };


        $scope.return = function(){
            $state.go('machine-type');
        }
    }

})();
