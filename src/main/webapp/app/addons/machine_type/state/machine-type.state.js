(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('machine-type', {
            parent: "root",
            url: "/app/machine_type",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/machine_type/view/machine-type.html',
                    controller: 'MachineTypeController'
                }
            }
        }).state('machine-type-details', {
            parent: "root",
            url: "/app/machine-type-details/:id",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/machine_type/view/machine-type-details.html',
                    controller: 'MachineTypeDetailsController'
                }
            }
        });
    }
})();
