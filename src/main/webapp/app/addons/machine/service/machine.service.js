
(function() {
    'use strict';
    var app = angular.module("maintenanceApp");
    app.factory("Machine", Machine);

    Machine.$inject = ['$resource'];

    function Machine($resource) {
        /* request */
        return $resource('http://localhost:8080' + '/api/machine/:id', {
            id: '@id'
        },{
            'findMachineByName' : {
                method: 'GET',
                url: 'api/machine/findByName',
                isArray: true
            }
        });
    }
})();
