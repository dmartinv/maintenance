(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('machine', {
            parent: "root",
            url: "/app/machine",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/machine/view/machine.html',
                    controller: 'MachineController'
                }
            }
        }).state('machine-details', {
            parent: "root",
            url: "/app/machine-details/:id",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/machine/view/machine-details.html',
                    controller: 'MachineDetailsController'
                }
            }
        });
    }
})();
