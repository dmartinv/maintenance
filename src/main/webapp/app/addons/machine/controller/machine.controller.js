(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MachineController', MachineController);

    MachineController.$inject = ['Machine', '$scope', '$state'];

    function MachineController (Machine, $scope, $state) {
        Machine.query().$promise.then(function(data){
            $scope.gridOptions.data  = data;
        });


        $scope.gridOptions = {
            columnDefs: [
                {name: 'id', field: 'id'},
                {name: 'Nombre', field: 'name'},
                {name: 'Serial', field: 'serialNumber'},
                {name: 'Fecha compra', field: 'adquisitionDate'},
                {name: 'Voltaje', field: 'voltage'},
                {name: 'Modelo', field: 'model'},
                {name: 'Descripción', field: 'description'},
                {
                    name: 'Edición',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.passObject(row.entity)"   class="btn btn-primary" >' +
                    '   editar' +
                    '</button>'
                },
                {
                    name: 'Borrar',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.delete(row.entity)"   class="btn btn-danger" >' +
                    '   borrar' +
                    '</button>'
                }
            ],
            minRowsToShow: 15,
            enableFiltering: true,
            enableSorting: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };

        $scope.passObject = function(entity){
            $state.go("machine-details", {id: entity.id});
        }

        $scope.delete = function(entity){
            Machine.delete({id: entity.id}).$promise.then(function(){
                $scope.addAlert('success', 'La máquina se ha eliminado');
                // Esta linea recarga una vez se hace la eliminación
                $state.go("machine", null, {reload: true});
            }, function(){
                $scope.addAlert('danger', 'La máquina no se ha eliminado');
            });
        }

    }
})();
