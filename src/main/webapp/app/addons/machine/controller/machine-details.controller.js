(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MachineDetailsController', MachineDetailsController);

    MachineDetailsController.$inject = ['Machine', '$scope', '$stateParams', '$state',
        'MachineType', 'MachineBrand', 'MachineCategory', 'Workplace'];

    function MachineDetailsController (Machine, $scope, $stateParams, $state,
                                       MachineType, MachineBrand, MachineCategory, Workplace) {
        $scope.entity = {};

        if(angular.isDefined($stateParams.id) && $stateParams.id !== "")
            Machine.get({id: $stateParams.id}).$promise.then(function(data){
                data.adquisitionDate = convertLocalDateFromServer(data.adquisitionDate);
                $scope.entity = data;
            });

        $scope.save = function(){
           Machine.save($scope.entity).$promise.then(function(entity){
                $scope.addAlert('success', 'La máquina ha sido creada/actualizada');
                $scope.entity = entity;
                $state.go('machine');
            }, function(){
                $scope.addAlert('danger', 'La máquina no ha sido creada/actualizada');
            });
        };

        $scope.findMachineTypeByName = function(val) {
            return MachineType.findMachineTypeByName({name: val}).$promise;
        };

        $scope.onSelectMachineType = function ($item) {
            $scope.entity.fkMachineType = $item;
        };

        $scope.findMachineBrandByName = function(val) {
            return MachineBrand.findMachineBrandByName({name: val}).$promise;
        };

        $scope.onSelectMachineBrand = function ($item) {
            $scope.entity.fkMachineBrand = $item;
        };

        $scope.findMachineCategoryByName = function(val) {
            return MachineCategory.findMachineCategoryByName({name: val}).$promise;
        };

        $scope.onSelectMachineCategory = function ($item) {
            $scope.entity.fkMachineCategory = $item;
        };

        $scope.findWorkplaceByName = function(val) {
            return Workplace.findWorkplaceByName({name: val}).$promise;
        };

        $scope.onSelectWorkplace = function ($item) {
            $scope.entity.fkWorkplace = $item;
        };


        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.popup2 = {
            opened: false
        };
        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };

        $scope.return = function(){
            $state.go('machine');
        }
    }
})();
