
(function() {
    'use strict';
    var app = angular.module("maintenanceApp");
    app.factory("MachineFailure", MachineFailure);

    MachineFailure.$inject = ['$resource'];

    function MachineFailure($resource) {
        /* request */
        return $resource('http://localhost:8080' + '/api/machine_failure/:id', {
            id: '@id'
        },{
            'findMachineFailureByName' : {
                method: 'GET',
                url: 'api/machine_failure/findByName',
                isArray: true
            }
        });
    }
})();
