(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MachineFailureController', MachineFailureController);

    MachineFailureController.$inject = ['MachineFailure', '$scope', '$state'];

    function MachineFailureController (MachineFailure, $scope, $state) {
        MachineFailure.query().$promise.then(function(data){
            $scope.gridOptions.data  = data;
        });


        $scope.gridOptions = {
            columnDefs: [
                {name: 'id', field: 'id'},
                {name: 'Nombre', field: 'name'},
                {name: 'Descripción', field: 'description'},
                {
                    name: 'Edición',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.passObject(row.entity)"   class="btn btn-primary" >' +
                    '   editar' +
                    '</button>'
                },
                {
                    name: 'Borrar',
                    width: 100,
                    cellTemplate: '<button ng-click="grid.appScope.delete(row.entity)"   class="btn btn-danger" >' +
                    '   borrar' +
                    '</button>'
                }
            ],
            minRowsToShow: 15,
            enableFiltering: true,
            enableSorting: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };

        $scope.passObject = function(entity){
            $state.go("machine-failure-details", {id: entity.id});
        }

        $scope.delete = function(entity){
            MachineFailure.delete({id: entity.id}).$promise.then(function(){
                $scope.addAlert('success', 'La falla de máquina se ha eliminado');
                // Esta linea recarga una vez se hace la eliminación
                $state.go("machine-failure", null, {reload: true});
            }, function(){
                $scope.addAlert('danger', 'La falla de máquina no se ha eliminado');
            });
        }

    }
})();
