(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .controller('MachineFailureDetailsController', MachineFailureDetailsController);

    MachineFailureDetailsController.$inject = ['MachineFailure', '$scope', '$stateParams', '$state'];

    function MachineFailureDetailsController (MachineFailure, $scope, $stateParams, $state) {
        $scope.entity = {};

        if(angular.isDefined($stateParams.id) && $stateParams.id !== "")
            MachineFailure.get({id: $stateParams.id}).$promise.then(function(data){
                $scope.entity = data;
            });

        $scope.save = function(){
            MachineFailure.save($scope.entity).$promise.then(function(entity){
                $scope.addAlert('success', 'La falla de máquina ha sido creado/actualizado');
                $scope.entity = entity;
                $state.go('machine-failure');
            }, function(){
                $scope.addAlert('danger', 'La falla de máquina no ha sido creado/actualizado');
            });
        };


        $scope.return = function(){
            $state.go('machine-failure');
        }
    }

})();
