(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('machine-failure', {
            parent: "root",
            url: "/app/machine_failure",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/machine_failure/view/machine-failure.html',
                    controller: 'MachineFailureController'
                }
            }
        }).state('machine-failure-details', {
            parent: "root",
            url: "/app/machine-failure-details/:id",
            views: {
                'principal@': {
                    templateUrl: 'app/addons/machine_failure/view/machine-failure-details.html',
                    controller: 'MachineFailureDetailsController'
                }
            }
        });
    }
})();
