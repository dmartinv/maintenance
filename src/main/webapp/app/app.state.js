(function() {
    'use strict';

    angular
        .module('maintenanceApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('root', {
            url: "/",
            views: {
                'navbar@': {
                    templateUrl: 'app/navbar/navbar.html'
                },
                'principal@': {
                    templateUrl: 'app/navbar/welcome.html'
                }
            }
        });
    }
})();
