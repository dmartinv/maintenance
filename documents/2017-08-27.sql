

-- [ Created objects ] --
-- object: public.employee | type: TABLE --
-- DROP TABLE IF EXISTS public.employee CASCADE;
CREATE TABLE public.employee(
	id serial NOT NULL,
	create_date timestamp,
	last_modified_date timestamp,
	name character varying,
	last_name character varying,
	identification character varying,
	gender character(11),
	job_position character varying,
	voltage real,
	fk_department integer,
	CONSTRAINT pk_employee PRIMARY KEY (id)

);


-- object: public.machine | type: TABLE --
-- DROP TABLE IF EXISTS public.machine CASCADE;
CREATE TABLE public.machine(
	id serial NOT NULL,
	create_date timestamp,
	last_modified_date timestamp,
	description character varying,
	serial_number character varying,
	machine_number character varying,
	adquisition_date date,
	voltage real,
	model character varying,
	fk_machine_type integer,
	fk_machine_brand integer,
	fk_machine_category integer,
	CONSTRAINT pk_machine PRIMARY KEY (id)

);


-- object: public.work_place | type: TABLE --
-- DROP TABLE IF EXISTS public.work_place CASCADE;
CREATE TABLE public.work_place(
	id serial NOT NULL,
	create_date timestamp,
	last_modified_date timestamp,
	name character varying,
	description character varying,
	CONSTRAINT pk_work_place PRIMARY KEY (id)

);


-- object: public.spare_part | type: TABLE --
-- DROP TABLE IF EXISTS public.spare_part CASCADE;
CREATE TABLE public.spare_part(
	id serial NOT NULL,
	create_date timestamp,
	last_modified_date timestamp,
	name character varying,
	description character varying,
	CONSTRAINT pk_spare_part PRIMARY KEY (id)

);

-- object: public.machine_type | type: TABLE --
-- DROP TABLE IF EXISTS public.machine_type CASCADE;
CREATE TABLE public.machine_type(
	id serial NOT NULL,
	create_date timestamp,
	last_modified_date timestamp,
	name character varying,
	description character varying,
	CONSTRAINT pk_machine_type PRIMARY KEY (id)

);


-- object: public.machine_category | type: TABLE --
-- DROP TABLE IF EXISTS public.machine_category CASCADE;
CREATE TABLE public.machine_category(
	id serial NOT NULL,
	create_date timestamp,
	last_modified_date timestamp,
	name character varying,
	description character varying,
	CONSTRAINT pk_machine_category PRIMARY KEY (id)

);

-- object: public.department | type: TABLE --
-- DROP TABLE IF EXISTS public.department CASCADE;
CREATE TABLE public.department(
	id serial NOT NULL,
	create_date timestamp,
	last_modified_date timestamp,
	name character varying,
	CONSTRAINT pk_department PRIMARY KEY (id)

);


-- object: public.request_spare_part | type: TABLE --
-- DROP TABLE IF EXISTS public.request_spare_part CASCADE;
CREATE TABLE public.request_spare_part(
	id serial NOT NULL,
	create_date timestamp,
	last_modified_date timestamp,
	fk_spare_part integer,
	CONSTRAINT pk_request_spare_part PRIMARY KEY (id)

);


-- object: public.maintenance | type: TABLE --
-- DROP TABLE IF EXISTS public.maintenance CASCADE;
CREATE TABLE public.maintenance(
	id serial NOT NULL,
	create_date timestamp,
	last_modified_date timestamp,
	fk_machine_failure integer,
	priority character varying,
	status character varying,
	name character varying,
	description character varying,
	fk_maintenance_type integer,
	standard_hours integer,
	due_date timestamp,
	start_date timestamp,
	fk_machine integer,
	CONSTRAINT pk_maintenance PRIMARY KEY (id)

);


-- object: public.maintenance_type | type: TABLE --
-- DROP TABLE IF EXISTS public.maintenance_type CASCADE;
CREATE TABLE public.maintenance_type(
	id serial NOT NULL,
	create_date timestamp,
	last_modified_date timestamp,
	name character varying,
	description character varying,
	standard_hours integer,
	CONSTRAINT pk_maintenance_type PRIMARY KEY (id)

);


-- object: public.machine_failure | type: TABLE --
-- DROP TABLE IF EXISTS public.machine_failure CASCADE;
CREATE TABLE public.machine_failure(
	id serial NOT NULL,
	create_date timestamp,
	last_modified_date timestamp,
	name character varying,
	description character varying,
	CONSTRAINT pk_machine_failure PRIMARY KEY (id)

);


-- object: public.schedule_maintenance | type: TABLE --
-- DROP TABLE IF EXISTS public.schedule_maintenance CASCADE;
CREATE TABLE public.schedule_maintenance(
	id serial NOT NULL,
	create_date timestamp,
	last_modified_date timestamp,
	date date,
	employee integer,
	CONSTRAINT pk_schedule_maintenance PRIMARY KEY (id)

);


-- object: public.maintenance_report | type: TABLE --
-- DROP TABLE IF EXISTS public.maintenance_report CASCADE;
CREATE TABLE public.maintenance_report(
	id serial NOT NULL,
	create_date timestamp,
	last_modified_date timestamp,
	name character varying,
	machines character varying,
	CONSTRAINT pk_maintenance_report PRIMARY KEY (id)

);


-- object: public.maintenance_task | type: TABLE --
-- DROP TABLE IF EXISTS public.maintenance_task CASCADE;
CREATE TABLE public.maintenance_task(
	id serial NOT NULL,
	create_date timestamp,
	last_modified_date timestamp,
	name character varying,
	description character varying,
	fk_maintenance integer,
	CONSTRAINT pk_maintenance_task PRIMARY KEY (id)

);


-- object: public.machine_brand | type: TABLE --
-- DROP TABLE IF EXISTS public.machine_brand CASCADE;
CREATE TABLE public.machine_brand(
	id serial NOT NULL,
	create_date timestamp,
	last_modified_date timestamp,
	name character varying,
	CONSTRAINT pk_machine_brand PRIMARY KEY (id)

);




-- [ Created foreign keys ] --
-- object: rel_employee_department | type: CONSTRAINT --
-- ALTER TABLE public.employee DROP CONSTRAINT IF EXISTS rel_employee_department CASCADE;
ALTER TABLE public.employee ADD CONSTRAINT rel_employee_department FOREIGN KEY (fk_department)
REFERENCES public.department (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_machine_machine_brand | type: CONSTRAINT --
-- ALTER TABLE public.machine DROP CONSTRAINT IF EXISTS rel_machine_machine_brand CASCADE;
ALTER TABLE public.machine ADD CONSTRAINT rel_machine_machine_brand FOREIGN KEY (fk_machine_brand)
REFERENCES public.machine_brand (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_machine_machine_type | type: CONSTRAINT --
-- ALTER TABLE public.machine DROP CONSTRAINT IF EXISTS rel_machine_machine_type CASCADE;
ALTER TABLE public.machine ADD CONSTRAINT rel_machine_machine_type FOREIGN KEY (fk_machine_type)
REFERENCES public.machine_type (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_machine_machine_categorie | type: CONSTRAINT --
-- ALTER TABLE public.machine DROP CONSTRAINT IF EXISTS rel_machine_machine_categorie CASCADE;
ALTER TABLE public.machine ADD CONSTRAINT rel_machine_machine_categorie FOREIGN KEY (fk_machine_category)
REFERENCES public.machine_category (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_request_spare_part_spare_part | type: CONSTRAINT --
-- ALTER TABLE public.request_spare_part DROP CONSTRAINT IF EXISTS rel_request_spare_part_spare_part CASCADE;
ALTER TABLE public.request_spare_part ADD CONSTRAINT rel_request_spare_part_spare_part FOREIGN KEY (fk_spare_part)
REFERENCES public.spare_part (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_maintenance_failure | type: CONSTRAINT --
-- ALTER TABLE public.maintenance DROP CONSTRAINT IF EXISTS rel_maintenance_failure CASCADE;
ALTER TABLE public.maintenance ADD CONSTRAINT rel_maintenance_failure FOREIGN KEY (fk_machine_failure)
REFERENCES public.machine_failure (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_maintenance_machine_type | type: CONSTRAINT --
-- ALTER TABLE public.maintenance DROP CONSTRAINT IF EXISTS rel_maintenance_machine_type CASCADE;
ALTER TABLE public.maintenance ADD CONSTRAINT rel_maintenance_machine_type FOREIGN KEY (fk_maintenance_type)
REFERENCES public.maintenance_type (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_maintenance_machine | type: CONSTRAINT --
-- ALTER TABLE public.maintenance DROP CONSTRAINT IF EXISTS rel_maintenance_machine CASCADE;
ALTER TABLE public.maintenance ADD CONSTRAINT rel_maintenance_machine FOREIGN KEY (fk_machine)
REFERENCES public.machine (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_maintenance_task_maintenance | type: CONSTRAINT --
-- ALTER TABLE public.maintenance_task DROP CONSTRAINT IF EXISTS rel_maintenance_task_maintenance CASCADE;
ALTER TABLE public.maintenance_task ADD CONSTRAINT rel_maintenance_task_maintenance FOREIGN KEY (fk_maintenance)
REFERENCES public.maintenance (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

