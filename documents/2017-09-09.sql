-- Database diff generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.8.2
-- PostgreSQL version: 9.5

-- [ Diff summary ]
-- Dropped objects: 1
-- Created objects: 9
-- Changed objects: 13
-- Truncated tables: 0

SET search_path=public,pg_catalog;
-- ddl-end --


-- [ Dropped objects ] --
ALTER TABLE public.machine DROP CONSTRAINT IF EXISTS rel_machine_spare_part CASCADE;
-- ddl-end --
ALTER TABLE public.employee DROP COLUMN IF EXISTS salary CASCADE;
-- ddl-end --
ALTER TABLE public.machine DROP COLUMN IF EXISTS machine_number CASCADE;
-- ddl-end --
ALTER TABLE public.machine DROP COLUMN IF EXISTS fk_spare_part CASCADE;
-- ddl-end --


-- [ Created objects ] --
-- object: voltage | type: COLUMN --
-- ALTER TABLE public.employee DROP COLUMN IF EXISTS voltage CASCADE;
ALTER TABLE public.employee ADD COLUMN voltage real;
-- ddl-end --


-- object: name | type: COLUMN --
-- ALTER TABLE public.machine DROP COLUMN IF EXISTS name CASCADE;
ALTER TABLE public.machine ADD COLUMN name character varying;
-- ddl-end --


-- object: fk_machine | type: COLUMN --
-- ALTER TABLE public.spare_part DROP COLUMN IF EXISTS fk_machine CASCADE;
ALTER TABLE public.spare_part ADD COLUMN fk_machine integer;
-- ddl-end --


-- object: request_date | type: COLUMN --
-- ALTER TABLE public.request_spare_part DROP COLUMN IF EXISTS request_date CASCADE;
ALTER TABLE public.request_spare_part ADD COLUMN request_date timestamp;
-- ddl-end --


-- object: delivery_date | type: COLUMN --
-- ALTER TABLE public.request_spare_part DROP COLUMN IF EXISTS delivery_date CASCADE;
ALTER TABLE public.request_spare_part ADD COLUMN delivery_date timestamp;
-- ddl-end --


-- object: fk_machine | type: COLUMN --
-- ALTER TABLE public.request_spare_part DROP COLUMN IF EXISTS fk_machine CASCADE;
ALTER TABLE public.request_spare_part ADD COLUMN fk_machine integer;
-- ddl-end --


-- object: units | type: COLUMN --
-- ALTER TABLE public.request_spare_part DROP COLUMN IF EXISTS units CASCADE;
ALTER TABLE public.request_spare_part ADD COLUMN units smallint;
-- ddl-end --




-- [ Changed objects ] --
ALTER TABLE public.employee ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE public.employee ALTER COLUMN id SET DEFAULT nextval('public.employee_id_seq'::regclass);
-- ddl-end --
ALTER TABLE public.machine ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE public.machine ALTER COLUMN id SET DEFAULT nextval('public.machine_id_seq'::regclass);
-- ddl-end --
ALTER TABLE public.work_place ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE public.work_place ALTER COLUMN id SET DEFAULT nextval('public.work_place_id_seq'::regclass);
-- ddl-end --
ALTER TABLE public.spare_part ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE public.spare_part ALTER COLUMN id SET DEFAULT nextval('public.spare_part_id_seq'::regclass);
-- ddl-end --
ALTER TABLE public.machine_type ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE public.machine_type ALTER COLUMN id SET DEFAULT nextval('public.machine_type_id_seq'::regclass);
-- ddl-end --
ALTER TABLE public.machine_category ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE public.machine_category ALTER COLUMN id SET DEFAULT nextval('public.machine_category_id_seq'::regclass);
-- ddl-end --
ALTER TABLE public.department ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE public.department ALTER COLUMN id SET DEFAULT nextval('public.department_id_seq'::regclass);
-- ddl-end --
ALTER TABLE public.request_spare_part ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE public.request_spare_part ALTER COLUMN id SET DEFAULT nextval('public.request_spare_part_id_seq'::regclass);
-- ddl-end --
ALTER TABLE public.maintenance ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE public.maintenance ALTER COLUMN id SET DEFAULT nextval('public.maintenance_id_seq'::regclass);
-- ddl-end --
ALTER TABLE public.maintenance_type ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE public.maintenance_type ALTER COLUMN id SET DEFAULT nextval('public.maintenance_type_id_seq'::regclass);
-- ddl-end --
ALTER TABLE public.machine_failure ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE public.machine_failure ALTER COLUMN id SET DEFAULT nextval('public.machine_failure_id_seq'::regclass);
-- ddl-end --
ALTER TABLE public.maintenance_task ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE public.maintenance_task ALTER COLUMN id SET DEFAULT nextval('public.maintenance_task_id_seq'::regclass);
-- ddl-end --
ALTER TABLE public.machine_brand ALTER COLUMN id TYPE integer;
-- ddl-end --
ALTER TABLE public.machine_brand ALTER COLUMN id SET DEFAULT nextval('public.machine_brand_id_seq'::regclass);
-- ddl-end --


-- [ Created foreign keys ] --
-- object: rel_spare_part_machine | type: CONSTRAINT --
-- ALTER TABLE public.spare_part DROP CONSTRAINT IF EXISTS rel_spare_part_machine CASCADE;
ALTER TABLE public.spare_part ADD CONSTRAINT rel_spare_part_machine FOREIGN KEY (fk_machine)
REFERENCES public.machine (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_request_spare_part_machine | type: CONSTRAINT --
-- ALTER TABLE public.request_spare_part DROP CONSTRAINT IF EXISTS rel_request_spare_part_machine CASCADE;
ALTER TABLE public.request_spare_part ADD CONSTRAINT rel_request_spare_part_machine FOREIGN KEY (fk_machine)
REFERENCES public.machine (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

